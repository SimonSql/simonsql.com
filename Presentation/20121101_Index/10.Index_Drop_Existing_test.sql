Use Study
go

select * 
  from [dbo].[sm_test_CL]

if object_id('sm_test_DE') is not null
	drop table sm_test_DE

create table sm_test_DE
(
i int identity(1,1)
, c1 varchar(10)
, c2 varchar(10)
)
create unique clustered index cl_sm_test_DE on sm_test_DE(i)
create nonclustered index nc01 on sm_test_DE (c1)
create nonclustered index nc02 on sm_test_DE (c2)

insert into sm_test_DE (c1, c2)
select top 1000000 ROW_NUMBER() over (order by getdate()) as c1, ROW_NUMBER() over (order by getdate()) as c2 
  from sys.syscolumns a
  cross join sys.syscolumns b
  cross join sys.syscolumns c

--Ctrl+M show actual Plan
create unique clustered index cl_sm_test_DE on sm_test_DE(i) with(drop_existing=on)
create unique clustered index cl_sm_test_DE on sm_test_DE(i) with(drop_existing=on, fillfactor=80, data_compression=page)
create unique clustered index cl_sm_test_DE on sm_test_DE(i, c1) with(drop_existing=on, fillfactor=80, data_compression=page)
create nonclustered index nc02 on sm_test_DE (c2) with(drop_existing=on, fillfactor=80)

--Showing drop clustered index
Drop index cl_sm_test_DE on sm_test_DE
Create unique clustered index cl_sm_test_DE on sm_test_DE(i)

-- Compare cost between drop + create and drop_existing
Drop index nc02 on sm_test_DE
go
create nonclustered index nc02 on sm_test_DE (c2) with(fillfactor=10)

create nonclustered index nc02 on sm_test_DE (c2) with(drop_existing=on, fillfactor=80)
