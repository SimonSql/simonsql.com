--With vs inner join vs cross apply

use Study
go

if object_id('test_Employee') is not null
	drop table test_Employee

select * 
  into study.dbo.test_Employee
  from [AdventureWorks].HumanResources.Employee

select top 10 * from test_Employee

create unique clustered index cl01 on test_Employee(EmployeeID)
create nonclustered index nc01 on test_Employee(managerID)


insert into study.dbo.test_Employee
select [NationalIDNumber],[ContactID],[LoginID],[ManagerID],[Title],[BirthDate],[MaritalStatus],[Gender],[HireDate],[SalariedFlag],[VacationHours],[SickLeaveHours],[CurrentFlag],[rowguid],[ModifiedDate]
  from [AdventureWorks].HumanResources.Employee
go 100


set transaction isolation level read uncommitted
set statistics io on
/*
How difference is between "With" and "Join"?
*/

;with emp as(
select a.*
  from test_Employee a
)
select a.EmployeeID, a.LoginID, b.EmployeeID, b.LoginID
  from emp a
  join test_Employee b on a.EmployeeID = b.ManagerID
 --where b.EmployeeID between 1 and 100

select a.EmployeeID, a.LoginID, b.EmployeeID, b.LoginID
  from test_Employee a
  join test_Employee b on a.EmployeeID = b.ManagerID
 --where b.EmployeeID between 1 and 100




/*
How difference is between "Join" and "Cross Apply"?
*/
select a.EmployeeID, a.LoginID, b.EmployeeID, b.LoginID
  from test_Employee a
  cross apply (select * from test_Employee b where a.EmployeeID = b.ManagerID) as b
 --where b.EmployeeID between 1 and 100

------------------------------------------------------------------------------------------------------

select a.EmployeeID, a.LoginID, b.EmployeeID, b.LoginID, c.EmployeeID, c.LoginID
  from test_Employee a
  join test_Employee b on a.EmployeeID = b.ManagerID
  join test_Employee c on b.EmployeeID = c.ManagerID
 where c.EmployeeID between 1 and 100


;with emp as(
select a.EmployeeID, a.LoginID, b.EmployeeID as b_EmployeeID, b.LoginID as b_LoginID
  from test_Employee a
  join test_Employee b on a.EmployeeID = b.ManagerID
)
select a.*, c.EmployeeID, c.LoginID
  from emp a
  join test_Employee c on a.b_EmployeeID = c.ManagerID
 where c.EmployeeID between 1 and 100


------------------------------------------------------------------------------------------------------

select a.EmployeeID, a.LoginID, b.EmployeeID, b.LoginID, c.EmployeeID, c.LoginID, d.EmployeeID, d.LoginID, e.EmployeeID, e.LoginID
  from test_Employee a
  join test_Employee b on a.EmployeeID = b.ManagerID
  join test_Employee c on b.EmployeeID = c.ManagerID
  join test_Employee d on c.EmployeeID = d.ManagerID
  join test_Employee e on d.EmployeeID = e.ManagerID
 where e.EmployeeID between 1 and 100


;with emp as(
select a.EmployeeID, a.LoginID, a.ManagerID
  from test_Employee a
)
select a.EmployeeID, a.LoginID, b.EmployeeID, b.LoginID, c.EmployeeID, c.LoginID, d.EmployeeID, d.LoginID, e.EmployeeID, e.LoginID
  from emp a
  join emp b on a.EmployeeID = b.ManagerID
  join emp c on b.EmployeeID = c.ManagerID
  join emp d on c.EmployeeID = d.ManagerID
  join emp e on d.EmployeeID = e.ManagerID
 where e.EmployeeID between 1 and 100

------------------------------------------------------------------------------------------------------

select a.EmployeeID, a.LoginID, b.EmployeeID, b.LoginID, c.EmployeeID, c.LoginID, d.EmployeeID, d.LoginID, e.EmployeeID, e.LoginID
  from test_Employee a
  join test_Employee b on a.EmployeeID = b.ManagerID
  join test_Employee c on b.EmployeeID = c.ManagerID
  join test_Employee d on c.EmployeeID = d.ManagerID
  join test_Employee e on d.EmployeeID = e.ManagerID
 where e.EmployeeID between 1 and 100
   and a.EmployeeID = 10


;with emp as(
select a.EmployeeID, a.LoginID
	 , b.EmployeeID as b_EmployeeID, b.LoginID as b_LoginID
	 , c.EmployeeID as c_EmployeeID, c.LoginID as c_LoginID
	 , d.EmployeeID as d_EmployeeID, d.LoginID as d_LoginID
  from test_Employee a
  join test_Employee b on a.EmployeeID = b.ManagerID
  join test_Employee c on b.EmployeeID = c.ManagerID
  join test_Employee d on c.EmployeeID = d.ManagerID
)
select a.*, e.EmployeeID, e.LoginID
  from emp a
  join test_Employee e on a.d_EmployeeID = e.ManagerID
 where e.EmployeeID between 1 and 100
   and a.EmployeeID = 10


select a.EmployeeID, a.LoginID, b.EmployeeID, b.LoginID, c.EmployeeID, c.LoginID, d.EmployeeID, d.LoginID, e.EmployeeID, e.LoginID
  from test_Employee a
  cross apply (select * from test_Employee b where a.EmployeeID = b.ManagerID) b
  cross apply (select * from test_Employee c where b.EmployeeID = c.ManagerID) c
  cross apply (select * from test_Employee d where c.EmployeeID = d.ManagerID) d
  cross apply (select * from test_Employee e where d.EmployeeID = e.ManagerID) e
 where e.EmployeeID between 1 and 100
   and a.EmployeeID = 10


