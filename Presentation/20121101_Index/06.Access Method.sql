use study
/*
1.Scan
<index seek>
Table Scan(Unordered Clustered Index Scan)
Unordered covering nonclustered index scan
Ordered clustered index scan
Ordered Covering Nonclustered Index Scan
<index Scan + Look up>
Nonclustered Index Seek + Ordered Partial Scan + Lookups

2. Seek
<index seek>
Unordered Nonclustered Index Scan + Lookups
Clustered Index Seek + Ordered Partial Scan
<index seek+Look up>
Covering Nonclustered Index Seek + Ordered Partial Scan
*/
set statistics io on

if OBJECT_ID('sm_test_CL') is not null
	drop table sm_test_CL

create table sm_test_CL
(i int identity(1,1)
,c1 int
,c2 int
)
go

insert into sm_test_CL (c1,c2)
select t.rowno+1, t.rowno+2
  from 
		(
		select ROW_NUMBER() over (order by getdate()) as rowno
		  from sys.all_columns a 
		 cross join sys.all_columns b
		) t
where rowno<=1000000
go

/*
exec sp_configure 'show advanced options',1
exec sp_configure 'max degree of parallelism',1
reconfigure with override
*/

/****************************
1.Table Scan(Unordered Clustered Index Scan)
****************************/
select top 10 * from sm_test_CL
order by i
option(maxdop 1)

/****************************
2.Unordered covering nonclustered index scan
****************************/

create		  nonclustered index nc01 on sm_test_CL (c1) with(pad_index=on, fillfactor=100)
go

select top 100 c1 from sm_test_CL

--select top 100 c1 from sm_test_CL
--order by c1

/****************************
3.Ordered clustered index scan
****************************/


set statistics profile on
go
create		  clustered index CL on sm_test_CL (i) with(pad_index=on, fillfactor=100)
go

--drop index CL on sm_test_CL


select top 10 * from sm_test_CL
order by i
option(maxdop 1)

/****************************
4.Ordered Covering Nonclustered Index Scan
****************************/

select top 10 c1 from sm_test_CL
order by c1
/****************************
5.Nonclustered Index Seek + Ordered Partial Scan + Lookups
****************************/

select * from sm_test_CL
where c1 between 1 and 30

/*
-- compare
set statistics profile off
select * from sm_test_CL
where c1 in (1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30)

--if object_id('tempdb.dbo.#t30') is not null
--	drop table #t30
--select *
--  into #t30
--  from sm_test_CL
-- where c1 in (1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30)

select a.*
  from sm_test_CL a
  join #t30 b on a.c1=b.c1


if object_id('tempdb.dbo.#t100') is not null
	drop table #t100
select top 100 c1
  into #t100
  from sm_test_CL
  
if object_id('tempdb.dbo.#t1000') is not null
	drop table #t1000
select top 1000 c1
  into #t1000
  from sm_test_CL

select a.*
  from sm_test_CL a
  join #t1000 b on a.c1=b.c1

select * from sm_test_CL
where c1 between 1 and 1000
  
select * from sm_test_CL
where c1<1300

select * from sm_test_CL
where c1<1347

select * from sm_test_CL
where c1<1348
  
select * from sm_test_CL
where c1<1400

http://www.sqlskills.com/blogs/kimberly/category/The-Tipping-Point.aspx
It's the point where the number of rows returned is "no longer selective enough".
SQL Server chooses NOT to use the nonclustered index to look up the corresponding data rows and instead performs a table scan.
It depends... it's MOSTLY tied to the number of pages in the table.
Generally, around 30% of the number of PAGES in the table is about where the tipping point occurs.
However, parallelism, some server settings (processor affinity and I/O affinity), memory and table size - all can have an impact.
And, since it can vary - I typically estimate somewhere between 25% and 33% as a rough tipping point.
*/

/****************************
6.Unordered Nonclustered Index Scan + Lookups
****************************/


create index nc02 on sm_test_CL (i,c2)
go

select * from sm_test_CL
where c2=1000


/****************************
7.Clustered Index Seek + Ordered Partial Scan
****************************/

select * from sm_test_CL
where i between 0 and 10

select * from sm_test_CL
where i in (1,2,3,4,5,6,7,8,9,10)


/*
--compare
select * from sm_test_CL
where i in (1,2,3,4,5,6,7,8,9,10)
order by c1
*/

/****************************
8.Covering Nonclustered Index Seek + Ordered Partial Scan
****************************/

select i, c1 from sm_test_CL
where c1 < 1000
/*
--compare
select i, c1, c2 from sm_test_CL
--where c1 < 1000
*/