--http://cafe.naver.com/sqlmvp.cafe?iframe_url=/ArticleRead.nhn%3Farticleid=522
--http://www.sqlskills.com/blogs/paul/post/Inside-the-Storage-Engine-Anatomy-of-a-record.aspx
/*

Next up in the Inside the Storage Engine series is a discussion of page structure. Pages exist to store records. A database page is an 8192-byte (8KB) chunk of a database data file. They are aligned on 8KB boundaries within the data files, starting at byte-offset 0 in the file. <?xml:namespace prefix = o ns = "urn:schemas-microsoft-com:office:office" /><?xml:namespace prefix = o />Here's a picture of the basic structure:

page

Header

The page header is 96 bytes long. What I'd like to do in this section is take an example page header dump from DBCC PAGE and explain what all the fields mean. I'm using the database from the page split post and I've snipped off the rest of the DBCC PAGE output.

    DBCC TRACEON (3604)

    DBCC PAGE ('pagesplittest', 1, 143, 1);

    GO

    m_pageId = (1:143)                   m_headerVersion = 1                  m_type = 1
    m_typeFlagBits = 0x4                 m_level = 0                          m_flagBits = 0x200
    m_objId (AllocUnitId.idObj) = 68     m_indexId (AllocUnitId.idInd) = 256 
    Metadata: AllocUnitId = 72057594042384384                                
    Metadata: PartitionId = 72057594038386688                                 Metadata: IndexId = 1
    Metadata: ObjectId = 2073058421      m_prevPage = (0:0)                   m_nextPage = (1:154)
    pminlen = 8                          m_slotCnt = 4                        m_freeCnt = 4420
    m_freeData = 4681                    m_reservedCnt = 0                    m_lsn = (18:116:25)
    m_xactReserved = 0                   m_xdesId = (0:0)                     m_ghostRecCnt = 0
    m_tornBits = 1333613242             

Here's what all the fields mean (note that the fields aren't quite stored in this order on the page):

    *
      m_pageId
          o
            This identifies the file number the page is part of and the position within the file. In this example, (1:143) means page 143 in file 1.
    *
      m_headerVersion
          o
            This is the page header version. Since version 7.0 this value has always been 1.
    *
      m_type
          o
            This is the page type. The values you're likely to see are:
                +
                  1 - data page. This holds data records in a heap or clustered index leaf-level.
                +
                  2 - index page. This holds index records in the upper levels of a clustered index and all levels of non-clustered indexes.
                +
                  3 - text mix page. A text page that holds small chunks of LOB values plus internal parts of text tree. These can be shared between LOB values in the same partition of an index or heap.
                +
                  4 - text tree page. A text page that holds large chunks of LOB values from a single column value.
                +
                  7 - sort page. A page that stores intermediate results during a sort operation.
                +
                  8 - GAM page. Holds global allocation information about extents in a GAM interval (every data file is split into 4GB chunks - the number of extents that can be represented in a bitmap on a single database page). Basically whether an extent is allocated or not. GAM = Global Allocation Map. The first one is page 2 in each file. More on these in a later post.
                +
                  9 - SGAM page. Holds global allocation information about extents in a GAM interval. Basically whether an extent is available for allocating mixed-pages. SGAM = Shared GAM. the first one is page 3 in each file. More on these in a later post.
                +
                  10 - IAM page. Holds allocation information about which extents within a GAM interval are allocated to an index or allocation unit, in SQL Server 2000 and 2005 respectively. IAM = Index Allocation Map. More on these in a later post.
                +
                  11 - PFS page. Holds allocation and free space information about pages within a PFS interval (every data file is also split into approx 64MB chunks - the number of pages that can be represented in a byte-map on a single database page. PFS = Page Free Space. The first one is page 1 in each file. More on these in a later post.
                +
                  13 - boot page. Holds information about the database. There's only one of these in the database. It's page 9 in file 1.
                +
                  15 - file header page. Holds information about the file. There's one per file and it's page 0 in the file.
                +
                  16 - diff map page. Holds information about which extents in a GAM interval have changed since the last full or differential backup. The first one is page 6 in each file.
                +
                  17 - ML map page. Holds information about which extents in a GAM interval have changed while in bulk-logged mode since the last backup. This is what allows you to switch to bulk-logged mode for bulk-loads and index rebuilds without worrying about breaking a backup chain. The first one is page 7 in each file.
    *
      m_typeFlagBits
          o
            This is mostly unused. For data and index pages it will always be 4. For all other pages it will always be 0 - except PFS pages. If a PFS page has m_typeFlagBits of 1, that means that at least one of the pages in the PFS interval mapped by the PFS page has at least one ghost record.
    *
      m_level
          o
            This is the level that the page is part of in the b-tree.
          o
            Levels are numbered from 0 at the leaf-level and increase to the single-page root level (i.e. the top of the b-tree).
          o
            In SQL Server 2000, the leaf level of a clustered index (with data pages) was level 0, and the next level up (with index pages) was also level 0. The level then increased to the root. So to determine whether a page was truly at the leaf level in SQL Server 2000, you need to look at the m_type as well as the m_level.
          o
            For all page types apart from index pages, the level is always 0.
    *
      m_flagBits
          o
            This stores a number of different flags that describe the page. For example, 0x200 means that the page has a page checksum on it (as our example page does) and 0x100 means the page has torn-page protection on it.
          o
            Some bits are no longer used in SQL Server 2005.
    *
      m_objId
    * m_indexId
          o
            In SQL Server 2000, these identified the actual relational object and index IDs to which the page is allocated. In SQL Server 2005 this is no longer the case. The allocation metadata totally changed so these instead identify what's called the allocation unit that the page belongs to (I'll do another post that describes these later today).
    *
      m_prevPage
    *
      m_nextPage
          o
            These are pointers to the previous and next pages at this level of the b-tree and store 6-byte page IDs.
          o
            The pages in each level of an index are joined in a doubly-linked list according to the logical order (as defined by the index keys) of the index. The pointers do not necessarily point to the immediately adjacent physical pages in the file (because of fragmentation).
          o
            The pages on the left-hand side of a b-tree level will have the m_prevPage pointer be NULL, and those on the right-hand side will have the m_nextPage be NULL.
          o
            In a heap, or if an index only has a single page, these pointers will both be NULL for all pages.
    *
      pminlen
          o
            This is the size of the fixed-length portion of the records on the page.
    *
      m_slotCnt
          o
            This is the count of records on the page.
    *
      m_freeCnt
          o
            This is the number of bytes of free space in the page.
    *
      m_freeData
          o
            This is the offset from the start of the page to the first byte after the end of the last record on the page. It doesn't matter if there is free space nearer to the start of the page.
    *
      m_reservedCnt
          o
            This is the number of bytes of free space that has been reserved by active transactions that freed up space on the page. It prevents the free space from being used up and allows the transactions to roll-back correctly. There's a very complicated algorithm for changing this value.
    *
      m_lsn
          o
            This is the Log Sequence Number of the last log record that changed the page.
    *
      m_xactReserved
          o
            This is the amount that was last added to the m_reservedCnt field.
    *
      m_xdesId
          o
            This is the internal ID of the most recent transaction that added to the m_reservedCnt field.
    *
      m_ghostRecCnt
          o
            The is the count of ghost records on the page.
    *
      m_tornBits
          o
            This holds either the page checksum or the bits that were displaced by the torn-page protection bits - depending on what form of page protection is turnde on for the database.

Note that I didn't include the fields starting with Metadata:. That's because they're not part of a page header. During SQL Server 2005 development I did some major work rewriting the guts of DBCC PAGE and to save everyone using it from having to do all the system table lookups to determine what the actual object and index IDs are, I changed DBCC PAGE to do them internally and output the results.

Records

See this blog post for details.

Slot Array

It's a very common misconception that records within a page are always stored in logical order. This is not true. There is another misconception that all the free-space in a page is always maintained in one contiguous chunk. This also is not true. (Yes, the image above shows the free space in one chunk and that very often  is the case for pages that are being filled gradually.)

If a record is deleted from a page, everything remaining on the page is not suddenly compacted - inserters pay the cost of compaction when its necessary, not deleters.

Consider a completely full page - this means that record deletions cause free space holes within the page. If a new record needs to be inserted onto the page, and one of the holes is big enough to squeeze the record into, why go to the bother of comapcting it? Just stick the record in and carry on. What if the record should logically have come at the end of all other records on the page, but we've just inserted it in the middle - doesn't that screw things up somewhat?

No, because the slot array is ordered and gets reshuffled as records are inserted and deleted from pages. As long as the first slot array entry points to the logically first record on the page, everything's fine. Each slot entry is just a two-byte pointer into the page - so its far more efficient to manipulate the slot array than it is to manipulate a bunch of records on the page. Only when we know there's enough free space contained within the page to fit in a record, but its spread about the page do we compact the records on the page to make the free space into a contiguous chunk.

One interesting fact is that the slot array grows backwards from the end of the page, so the free space is squeezed from the top by new rows, and from the bottom by the slot array.
*/
-- dbcc traceon(2520)
-- dbcc help('?')
-- dbcc help(ind)


-- sql server 2000 detail version 2187
-- 데이터베이스 생성
use master
go

if exists (select * from master.dbo.sysdatabases where name ='db_indextest')
begin
alter database db_indextest set single_user with rollback immediate
drop database db_indextest
create database db_indextest
end
else
begin
create database db_indextest
end
go

use db_indextest
go


create table tblx
(idx char(90)
, col1 char(90) -- index max size = 90 byte in sql server 2000
, col2 char(90)
, col3 char(90)
)
go

set nocount on
begin tran

declare @idx int
set @idx = 500000

while (@idx > 0)
begin
insert into tblx (idx, col1, col2, col3) values
(@idx, @idx, @idx, 1)
set @idx = @idx - 1
end

commit tran
go

select top 10 * from tblx
go

-- heap 테이블은 테스트에서 제외한다.
create clustered index cl_tblx_idx on tblx(idx)
go

-- 유니크 인덱스를 만든다 이넘은 non leaf 구성이 다르다
create unique nonclustered index unc_tblx_col1 on tblx(col1)
go

-- 유니크 제한이 없고 값은 다 다른값이 저장되어 있다. 이넘은 모두 중복된
-- 컬럼과 non leaf 구조가 같다.
create nonclustered index nc_tblx_col2_d on tblx(col2)
go

-- 모두 중복된 구조이다.
create nonclustered index nc_tblx_col3_s on tblx(col3)


dbcc help(showcontig) -- 머리 나빠서 맨날 쳐본다.
-- showcontig (table_id | table_name [, index_id | index_name] [WITH FAST, ALL_INDEXES, TABLERESULTS [,ALL_LEVELS]])
dbcc showcontig (tblx, unc_tblx_col1) with tableresults
-- 11629 개의 page 사용
dbcc showcontig (tblx, nc_tblx_col2_d) with tableresults
-- 11629 개의 page 사용
dbcc showcontig (tblx, nc_tblx_col3_s) with tableresults
-- 11628 개의 page 사용

-- 이 dbcc showcontig 명령에서 나오는 page 는 IAM page 를 제외한 leaf 페이지 입니다.
-- leaf 만 오직 나온다는 겁니다. 다음에서 그 이유를 알 수 있습니다.

select name, indid from dbo.sysindexes where id = object_id ('tblx')

--cl_tblx_idx 1
--unc_tblx_col1 2
--nc_tblx_col2_d 3
--nc_tblx_col3_s 4

select indexproperty(object_id('tblx'), 'unc_tblx_col1', 'indexdepth')
select indexproperty(object_id('tblx'), 'nc_tblx_col2_d', 'indexdepth')
select indexproperty(object_id('tblx'), 'nc_tblx_col3_s', 'indexdepth')
-- 의 명령어로 총 인덱스의 depth 를 구할 수 있음
-- 모두 4개의 depth 로 이루어져 있음

dbcc help(ind) -- 또 쳐본다. 맨날 까먹는다.
-- ind ( { 'dbname' | dbid }, { 'objname' | objid }, { indid | 0 | -1 | -2 } )
-- dbcc ind('db_indextest', 'tblx', -2) -- 모든 인덱스 정보 표시 pageType 10번 iam page 만 출력

dbcc ind('study', 'tblx', 2)
dbcc ind('study', 'tblx', 3)
dbcc ind('study', 'tblx', 4)

nc_tblx_col1 11786 개
nc_tblx_col2_d 11923 개
nc_tblx_col3_s 11921 개

-- 간단한 쿼리를 위해서 아래와 같은 테크닉을 쓴다.

-- 1 유니크 조건을 준 인덱스
if object_id('tempdb..#ind_result') is not null
drop table #ind_result

create table #ind_result
(PageFID bigint, PagePID bigint
, IAMFID bigint, IAMPID bigint
, ObjectID bigint, IndexID bigint
, PartitionNumber	int, PartitionID	bigint, iam_chain_type varchar(255) --SQL2008 only
, PageType bigint, IndexLevel bigint
, NextPageFID bigint, NextPagePID bigint
, PrevPageFID bigint, PrevPagePID bigint)
go

insert into #ind_result
exec ('dbcc ind(''study'', ''tblx'', 2)')

select * from #ind_result order by indexLevel  desc 

select indexLevel, count(*)
from #ind_result
group by indexLevel
order by 1

0 11630
1 152
2 3
3 1

-- 값만 모두 다른 인덱스
if object_id('tempdb..#ind_result') is not null
drop table #ind_result

create table #ind_result
(PageFID bigint, PagePID bigint
, IAMFID bigint, IAMPID bigint
, ObjectID bigint, IndexID bigint
, PartitionNumber	int, PartitionID	bigint, iam_chain_type varchar(255) --SQL2008 only
, PageType bigint, IndexLevel bigint
, NextPageFID bigint, NextPagePID bigint
, PrevPageFID bigint, PrevPagePID bigint)
go

insert into #ind_result
exec ('dbcc ind(''study'', ''tblx'', 3)')

select * from #ind_result order by indexLevel  desc 

select indexLevel, count(*)
from #ind_result
group by indexLevel
order by 1

0 11630
1 284
2 8
3 1

-- 모두 중복된 값이 들어가 있는 인덱스
if object_id('tempdb..#ind_result') is not null
drop table #ind_result

create table #ind_result
(PageFID bigint, PagePID bigint
, IAMFID bigint, IAMPID bigint
, ObjectID bigint, IndexID bigint
, PartitionNumber	int, PartitionID	bigint, iam_chain_type varchar(255) --SQL2008 only
, PageType bigint, IndexLevel bigint
, NextPageFID bigint, NextPagePID bigint
, PrevPageFID bigint, PrevPagePID bigint)
go

insert into #ind_result
exec ('dbcc ind(''study'', ''tblx'', 4)')

select * from #ind_result order by indexLevel  desc 

select indexLevel, count(*)
from #ind_result
group by indexLevel
order by 1

0 11629
1 284
2 7
3 1

select * from #ind_result


dbcc help(page)
page ( {'dbname' | dbid}, filenum, pagenum [, printopt={0|1|2|3} ][, cache={0|1} ])

dbcc traceon (3604)


dbcc page('db_indextest', 1, 41408, 3, 0)
FileId PageId      Row    Level  ChildFileId ChildPageId col1
------ ----------- ------ ------ ----------- ----------- -------
1      41408       0      2      1           29338       NULL
1      41408       1      2      1           41407       102978                                                                                   
1      41408       2      2      1           41742       105958                                                                                   
1      41408       3      2      1           42076       108938                                                                                   
1      41408       4      2      1           42282       111917                                                                                   

dbcc page('db_indextest', 1, 9764, 3, 0)
FileId PageId      Row    Level  ChildFileId ChildPageId
col2                                                                                      
idx                                                                                        ?
------ ----------- ------ ------ ----------- ----------- --------------------------------------------------
---------------------------------------- ----------------------------------------------------------------
-------------------------- -----------
1      9764        0      2      1           9690        31697                                                                                     
31697                                                                                      NULL
1      9764        1      2      1           9763        318556                                                                                    
318556                                                                                     NULL
1      9764        2      2      1           9806        320141                                                                                    
320141                                                                                     NULL
1      9764        3      2      1           9912        321729                                                                                    
321729                                                                                     NULL
1      9764        4      2      1           9986        323315                                                                                    
323315                                                                                     NULL

dbcc page('db_indextest', 1, 21601, 3, 0)
FileId PageId      Row    Level  ChildFileId ChildPageId
col3                                                                                      
idx                                                                                        ?
------ ----------- ------ ------ ----------- ----------- --------------------------------------------------
---------------------------------------- ----------------------------------------------------------------
-------------------------- -----------
1      21601       0      2      1           21600       1                                                                                         
230106                                                                                     NULL
1      21601       1      2      1           21643       1                                                                                         
231694                                                                                     NULL
1      21601       2      2      1           21685       1                                                                                         
233280                                                                                     NULL
1      21601       3      2      1           21727       1                                                                                         
234868                                                                                     NULL
1      21601       4      2      1           21769       1                                                                                         
236454                                                                                     NULL


-- 이러한 현상은 unique index 의 중요성을 다시한번 부각 시킨다.
-- unique 인덱스는 실제 중복된 값이 들어가는 값을 막아 주므로 클러스터 인덱스 값을
-- nonleaf level 에서 가지고 있을 필요가 없다.
-- 또한 인덱스 leaf 페이지는 당연히 모두 같아야 한다.
-- 또한 not null null 에 따라서도 틀려지게 된다. null 을 허용하면 null bitmap 을 삽입해야
-- 하는것 또한 자명한 사실이다. 또한 매우 효율적인 구조여서 우리가 이러한 것을 모두 고려하지
-- 않아도 손실이 정말 작은 것을 알 수 있다.
-- 테스트의 효율성을 높이기 위해서 index 는 90 byte 짜리 인덱스를 사용했다.

-- 시간 되시면 아래 글을 보고 우리 테이블에서 실제로 unique 를 줄 수 있는데 안주고 있는 인덱스는
-- 몇개나 있는지 찾아보시면 좋은 시간이 될 것 같습니다.

http://sqler.pe.kr/web_board/view_list.asp?id=1008&read=1815&pagec=4&gotopage=4&block=0&part=myboard7&tip=

exec [ASP_DATABASE_INDEX_SIZE_N_INFO]
exec sp_AllocationMetadata 'sm_test_CL'
exec sp_AllocationMetadata 'sm_test_uCL'