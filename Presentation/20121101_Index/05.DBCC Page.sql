dbcc page ( {'dbname' | dbid}, filenum, pagenum [, printopt={0|1|2|3} ])
dbcc page (dbname, pageno,[printopt [,cache [,logical [,cachename]]]])

dbid      - database ID
dbname    - database name
pageno    - page number
printopt  - output format:
            0 - print buffer and page header only (default)
            1 - print buffer and page headers, rows and
                offset table
            2 - print buffer and page headers, hex dump
                of data and offset table
cache     - where to get the page:
            0 - read page from disk
            1 - read page from cache if present, otherwise
                read from disk (default)
logical   - the page type
            0 - pageno is a virtual page
            1 - pageno is a logical page (default)
cachename - the cache name
           -1 - all caches


The filenum and pagenum parameters are taken from the page IDs 
that come from various system tables and appear in DBCC or other system error messages.
A page ID of, say, (1:354) has filenum = 1 and pagenum = 354.

The printopt parameter has the following meanings:

0 - print just the page header
1 - page header plus per-row hex dumps and a dump of the page slot array 
	(unless its a page that doesn''t have one, like allocation bitmaps)
2 - page header plus whole page hex dump
3 - page header plus detailed per-row interpretation

The per-row interpretation work for all page types, including allocation bitmaps.
By default, the output is sent to the errorlog. 
If you want the output to come back to your current connection, turn on trace flag 3604.