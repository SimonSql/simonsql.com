use study

/*
--8000 byte test
create table sm_test
(c char(3000)
,d char(7000)
)

--Extent page allocation test
create table sm_test
(i int identity(1,1)
,c char(8000)
)

insert into sm_test
values (REPLICATE('c',8000))
go
exec sp_spaceused sm_test

*/
if OBJECT_ID('sm_test_CL') is not null
	drop table sm_test_CL

create table sm_test_CL
(i int identity(1,1)--default(0) -- 
,c1 int
,c2 int
)
go

if OBJECT_ID('sm_test_uCL') is not null
	drop table sm_test_uCL

create table sm_test_uCL
(i int identity(1,1)
,c1 int
,c2 int
)
go

create		  clustered index CL on sm_test_CL (i) --with(pad_index=on, fillfactor=100)
create unique clustered index uCL on sm_test_uCL (i) --with(pad_index=on, fillfactor=100)

create unique nonclustered index uNC01 on sm_test_CL (i) include (c1,c2) --with(pad_index=on, fillfactor=100)
create		  nonclustered index NC02 on sm_test_CL (i) include (c1,c2) --with(pad_index=on, fillfactor=100)

create unique nonclustered index uNC01 on sm_test_uCL (i) include (c1,c2) --with(pad_index=on, fillfactor=100)
create		  nonclustered index NC02 on sm_test_uCL (i) include (c1,c2) --with(pad_index=on, fillfactor=100)

set statistics io on
set statistics time on

exec sp_SQLskills_SQL2008_helpindex  'sm_test_CL'
exec sp_SQLskills_SQL2008_helpindex  'sm_test_uCL'



/* Actual Size difference */
if OBJECT_ID('sm_test_CL') is not null
	drop table sm_test_CL

create table sm_test_CL
(i int identity(1,1)--default(0) -- 
,c1 int
,c2 int
)
go

if OBJECT_ID('sm_test_uCL') is not null
	drop table sm_test_uCL

create table sm_test_uCL
(i int identity(1,1)
,c1 int
,c2 int
)
go


insert into sm_test_CL (c1,c2)
select t.rowno+1, t.rowno+2
  from 
		(
		select ROW_NUMBER() over (order by getdate()) as rowno
		  from sys.all_columns a 
		 cross join sys.all_columns b
		) t
where rowno<=1000000
go

insert into sm_test_uCL (c1,c2)
select t.rowno+1, t.rowno+2
  from 
		(
		select ROW_NUMBER() over (order by getdate()) as rowno
		  from sys.all_columns a 
		 cross join sys.all_columns b
		) t
where rowno<=1000000
go

select * from sm_test_CL
--select distinct i from sm_test_CL
select COUNT(*) from sm_test_CL

select * from sm_test_uCL
select COUNT(*) from sm_test_uCL

select top 100 * from sm_test_CL
select top 100 * from sm_test_uCL

exec sp_SQLskills_SQL2008_helpindex  'sm_test_CL'
exec sp_SQLskills_SQL2008_helpindex  'sm_test_uCL'

create nonclustered index NC01 on sm_test_CL (c1) with(pad_index=on, fillfactor=100)
create unique nonclustered index uNC01 on sm_test_CL (c2) with(pad_index=on, fillfactor=100)

create nonclustered index NC01 on sm_test_uCL (c1) with(pad_index=on, fillfactor=100)
create unique nonclustered index uNC01 on sm_test_uCL (c2) with(pad_index=on, fillfactor=100)

dbcc traceon(3604)
--DBCC ind ('study','sm_test_CL',-2)
--DBCC ind ('study','sm_test_CL',-1)
--DBCC ind ('study','sm_test_CL',0) --Heap
DBCC ind ('study','sm_test_CL',1) 
--DBCC ind ('study','sm_test_CL',2)
DBCC ind ('study','sm_test_uCL',1)

DBCC page('study',1,379,3) WITH TABLERESULTS
DBCC page('study',1,396,3) WITH TABLERESULTS



set statistics io on
set statistics time on
--set statistics time off
set showplan_all on
--set showplan_all off


select * from Study.dbo.sm_test_CL
go
select COUNT(*) from sm_test_CL


select * from sm_test_uCL
select COUNT(*) from sm_test_uCL

set statistics io off

insert into sm_test with(tablock)
select t.rowno+1
  from 
		(
		select ROW_NUMBER() over (order by getdate()) as rowno
		  from sys.all_columns a 
		 cross join sys.all_columns b
		) t
where rowno>1000000 and rowno<=5000000

exec sp_SQLskills_SQL2008_helpindex  'sm_test_CL'
exec sp_SQLskills_SQL2008_helpindex  'sm_test_uCL'
exec sp_AllocationMetadata 'sm_test_CL'

exec sp_spaceused sm_test_CL
exec sp_spaceused sm_test_uCL