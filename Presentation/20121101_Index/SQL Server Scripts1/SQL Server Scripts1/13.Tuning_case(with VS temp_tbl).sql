Use WFS
go
/*
--WFS.[Account].[usp_QueueDarkDealers]
-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.

	SET NOCOUNT ON;

	if object_id('tempdb.dbo.#ISRDivisionStates') is not null
		drop table #ISRDivisionStates
	if object_id('tempdb.dbo.#HistoricalRepAssignment') is not null
		drop table #HistoricalRepAssignment

	DECLARE @Active SMALLINT = 60, 
			@QueuedToday SMALLINT = 2, 
			@QueuedNotContacted SMALLINT = 6,
			@DaysSinceLastRepActivity SMALLINT = 30,
			@DaysSinceLastQueued SMALLINT = 30,
			@TimesToQueueDealer SMALLINT = 3,
			@CreatedBy SMALLINT = 1 /* System */,
			@MailServer VARCHAR(20) = 'mail.nowcom.com',
			@Port SMALLINT = 25,
			@DefaultEmail VARCHAR(20) = 'rob@nowcom.com'
			
	DECLARE @Today DATETIME
	SET @Today = GETDATE()		
	
	IF (9 - DATEPART(hh, @Today)) > 0
		SET @Today = DATEADD(hh, 1 * (9 - DATEPART(hh, @Today)), @today)
	SET @Today = DATEADD(mi, 45, @Today)
					

	SELECT	AccountID, CAST(NewValue AS INT) AS HRAQueuedRepID, MAX(CreatedDate) QueuedRepAssignedDate 
	INTO	#HistoricalRepAssignment
	FROM	Account.EventLogs  WITH (NOLOCK)
	WHERE	EventTypeID = 13 
	GROUP BY AccountID, NewValue
		
	-- 1. Create ISR Divison States table
	--CREATE TABLE #ISRDivisionStates(ISRDivisionID SMALLINT, DivisionState VARCHAR(100))  
	--INSERT INTO #ISRDivisionStates	-- drop table #ISRDivisionStates
	--VALUES	(1, 'AK,WA,OR,NV,ID,MT,UT,AZ,CA,HI'),
	--		(2, 'ND,SD,WY,NE,IA,CO,KS,MO,NM,OK,AR,TX,MN,WI,MI,IL,IN,OH,KY'),
	--		(3, 'ME,NH,VT,NY,MA,CT,RI,PA,DE,WV,VA,MD,TN,NC,SC,GA,LA,MS,AL,FL,NJ')
*/

--select * from #HistoricalRepAssignment

SET STATISTICS TIME ON
SET STATISTICS IO ON

	DECLARE @DaysSinceLastRepActivity SMALLINT = 30, @TimesToQueueDealer SMALLINT = 3, @DaysSinceLastQueued SMALLINT = 30
	-- 3. Get dark dealers
	;WITH Account AS
	(
		SELECT	AccountID, ParentAccountID,
				MAX(CASE WHEN T1.AssignedToUserID = A.SalesRepID THEN ActivityCountForRep ELSE NULL END) AS ActivityCountForSalesRep, 
				MAX(CASE WHEN T1.AssignedToUserID = A.QueuedRepID THEN ActivityCountForRep ELSE NULL END) AS ActivityCountForQueuedRep, 
				MAX(CASE WHEN T2.CreatedBy = A.SalesRepID THEN NoteCountForRep ELSE NULL END) AS NoteCountForSalesRep, 
				MAX(CASE WHEN T2.CreatedBy = A.QueuedRepID THEN NoteCountForRep ELSE NULL END) AS NoteCountForQueuedRep
		FROM	Account.Accounts A WITH (NOLOCK)
				-- Get activity count of sales rep and queued sales rep within 30 days (if the child accounts have activities, exclude the parent account)
				LEFT JOIN 
				(	SELECT	PrimaryEntityID, AssignedToUserID, COUNT(*) ActivityCountForRep 
					FROM	Activity.Activities ACT WITH (NOLOCK)
					WHERE	CreatedDate >= GETUTCDATE() - @DaysSinceLastRepActivity
					GROUP BY PrimaryEntityID, AssignedToUserID 
				) T1 ON A.AccountID = T1.PrimaryEntityID AND (T1.AssignedToUserID IN (A.SalesRepID, A.QueuedRepID))
				-- Get note count of sales rep and queued sales rep within 30 days (if the child accounts have notes, exclude the parent account)
				LEFT OUTER JOIN		
				(	SELECT	EntityID, CreatedBy, COUNT(*) NoteCountForRep 
					FROM	Account.Notes WITH (NOLOCK)
					WHERE	CreatedDate >= GETUTCDATE() - @DaysSinceLastRepActivity
					GROUP BY EntityID, CreatedBy
				) T2 ON A.AccountID = T2.EntityID AND (T2.CreatedBy IN (A.SalesRepID, A.QueuedRepID))
		WHERE	A.ProductionStatusID = 4 /* 4:Dark */ AND A.DealerStatusID <> 5 AND ISNULL(IsDeleted, 0) <> 1 AND ISNULL(IsTestAccount, 0) <> 1 
		GROUP BY AccountID,  A.ParentAccountID
	) 

	SELECT	ROW_NUMBER() OVER (ORDER BY CASE WHEN(A.SalesRepID IS NOT NULL AND NowcomAccountStatus = 'Active' AND P.LastDealDate IS NOT NULL) THEN 0 
												WHEN(A.SalesRepID IS NOT NULL AND NowcomAccountStatus = 'Active') THEN 1
												WHEN(NowcomAccountStatus = 'Active' AND P.LastDealDate IS NOT NULL) THEN 2
												WHEN(NowcomAccountStatus = 'Active') THEN 3
												WHEN(A.SalesRepID IS NOT NULL AND P.LastDealDate IS NOT NULL AND NowcomAccountStatus IS NOT NULL AND NowcomAccountStatus <> 'Active') THEN 4
												WHEN(A.SalesRepID IS NOT NULL AND NowcomAccountStatus IS NOT NULL AND NowcomAccountStatus <> 'Active') THEN 5
												WHEN(A.SalesRepID IS NOT NULL AND P.LastDealDate IS NOT NULL) THEN 6
												WHEN(A.SalesRepID IS NOT NULL) THEN 7
												ELSE 99 END, ISNULL(P.LastDealDate, '1/1/1900') DESC) AS Priority,
			A.AccountID, T.ParentAccountID, A.DealerCode, A.AccountName /* DealerDBA ?*/ AS DealerName, A.RegionID, 
			ISNULL(C.FirstName + ' ', '') + ISNULL(C.LastName + ' - ', '') + ISNULL(J.Name, '') AS PrimaryName,
			ISNULL(C.Phone, '') AS PrimaryPhone, A.SalesRepID AS AssignedRepID, U.FullName AS AssignedRep, A.QueuedRepID,
			P.LastDealDate, ADDR.State AS DealerState, ADDR.Longitude, ADDR.Latitude, 
			'http://www.google.com/gmm/x?action=ROUT&end=' + REPLACE(ISNULL(ADDR.Address1, ''), ' ', '%20') 
				+ CASE WHEN (ADDR.Address2 IS NOT NULL) THEN '%20' + REPLACE(ADDR.Address2, ' ', '%20') ELSE '' END
				+ ',%20' + REPLACE(ISNULL(ADDR.City, ''), ' ', '%20') + ',%20' + REPLACE(ISNULL(ADDR.State, ''), ' ', '%20') + 
				'%20' + REPLACE(ISNULL(ADDR.Zip, ''), ' ', '%20') AS BlackberryLink, 
			N.NowcomAccountStatus, QC.DaysSinceLastQueued, QC.QueuedCount						
	--INTO	#DarkDealers -- drop table #DarkDealers
	FROM	Account T
			INNER JOIN Account.Accounts A ON T.AccountID = A.AccountID 
			LEFT OUTER JOIN Account.Address ACADR ON A.AccountID = ACADR.AccountID AND AddressTypeID = 3 /* 3: Dealer */
			LEFT OUTER JOIN Address.Address ADDR ON ACADR.AddressID = ADDR.AddressID	
			LEFT OUTER JOIN Account.NowcomInformation N ON T.AccountID = N.AccountID 
			LEFT OUTER JOIN Security.Users U ON A.SalesRepID = U.UserID
			LEFT OUTER JOIN Account.Performance P ON A.AccountID = P.AccountID
			LEFT OUTER JOIN Account.Contacts C ON A.AccountID = C.AccountID AND IsPrimaryContact = 1 AND IsEnabled = 1
			LEFT OUTER JOIN Lookup.JobTitle J ON C.JobTitleID = J.JobTitleID			
			LEFT OUTER JOIN		-- Get the number of Queued Rep Assignment since going dark
			(	
				SELECT	ISNULL(P.AccountID, L.AccountID) AS AccountID, 
						SUM(CASE WHEN L.QueuedRepAssignedDate > ISNULL(P.LastDealDate, '01/01/1900') THEN 1 ELSE 0 END) QueuedCount, 
						DATEDIFF(DAY, MAX(QueuedRepAssignedDate), GETDATE()) AS DaysSinceLastQueued
				FROM	Account.Performance P
						LEFT OUTER JOIN #HistoricalRepAssignment L ON P.AccountID = L.AccountID
				GROUP BY ISNULL(P.AccountID, L.AccountID)
			) QC ON T.AccountID = QC.AccountID
	WHERE ( ( ActivityCountForSalesRep IS NULL AND NoteCountForSalesRep IS NULL AND A.QueuedRepID IS NULL)
				OR ( ActivityCountForQueuedRep IS NULL AND NoteCountForQueuedRep IS NULL  AND A.QueuedRepID IS NOT NULL) )
			AND A.AccountID NOT IN 
			(	
				SELECT DISTINCT ParentAccountID 
				FROM	Account 
				WHERE	ParentAccountID IS NOT NULL 
						AND NOT (   ( ActivityCountForSalesRep IS NULL AND NoteCountForSalesRep IS NULL AND A.QueuedRepID IS NULL)
									OR ( ActivityCountForQueuedRep IS NULL AND NoteCountForQueuedRep IS NULL  AND A.QueuedRepID IS NOT NULL) )
			)
			AND ISNULL(QC.QueuedCount, 0) < @TimesToQueueDealer
			AND ISNULL(DaysSinceLastQueued, 999) > @DaysSinceLastQueued
			AND NOT EXISTS ( SELECT TOP 1 1 FROM Deal.GeneralInfo WHERE DealStatusID = 2 AND AccountID = T.AccountID ) /* No Pending Deal : DealStatusID =2(Pending) */
			AND T.ParentAccountID IS NULL
	OPTION (MAXDOP 1)

GO
print '------------------------------------------------'

	DECLARE @DaysSinceLastRepActivity SMALLINT = 30, @TimesToQueueDealer SMALLINT = 3, @DaysSinceLastQueued SMALLINT = 30

	IF OBJECT_ID('tempdb.dbo.#Account') IS NOT NULL
		DROP TABLE #Account
	SELECT	AccountID, ParentAccountID,
			MAX(CASE WHEN T1.AssignedToUserID = A.SalesRepID THEN ActivityCountForRep ELSE NULL END) AS ActivityCountForSalesRep, 
			MAX(CASE WHEN T1.AssignedToUserID = A.QueuedRepID THEN ActivityCountForRep ELSE NULL END) AS ActivityCountForQueuedRep, 
			MAX(CASE WHEN T2.CreatedBy = A.SalesRepID THEN NoteCountForRep ELSE NULL END) AS NoteCountForSalesRep, 
			MAX(CASE WHEN T2.CreatedBy = A.QueuedRepID THEN NoteCountForRep ELSE NULL END) AS NoteCountForQueuedRep
	INTO #Account
	FROM	Account.Accounts A WITH (NOLOCK)
			-- Get activity count of sales rep and queued sales rep within 30 days (if the child accounts have activities, exclude the parent account)
			LEFT JOIN 
			(	SELECT	PrimaryEntityID, AssignedToUserID, COUNT(*) ActivityCountForRep 
				FROM	Activity.Activities ACT WITH (NOLOCK)
				WHERE	CreatedDate >= GETUTCDATE() - @DaysSinceLastRepActivity
				GROUP BY PrimaryEntityID, AssignedToUserID 
			) T1 ON A.AccountID = T1.PrimaryEntityID AND (T1.AssignedToUserID IN (A.SalesRepID, A.QueuedRepID))
			-- Get note count of sales rep and queued sales rep within 30 days (if the child accounts have notes, exclude the parent account)
			LEFT OUTER JOIN		
			(	SELECT	EntityID, CreatedBy, COUNT(*) NoteCountForRep 
				FROM	Account.Notes WITH (NOLOCK)
				WHERE	CreatedDate >= GETUTCDATE() - @DaysSinceLastRepActivity
				GROUP BY EntityID, CreatedBy
			) T2 ON A.AccountID = T2.EntityID AND (T2.CreatedBy IN (A.SalesRepID, A.QueuedRepID))
	WHERE	A.ProductionStatusID = 4 /* 4:Dark */ AND A.DealerStatusID <> 5 AND ISNULL(IsDeleted, 0) <> 1 AND ISNULL(IsTestAccount, 0) <> 1 
	GROUP BY AccountID,  A.ParentAccountID
	OPTION (MAXDOP 1)


	SELECT	ROW_NUMBER() OVER (ORDER BY CASE WHEN(A.SalesRepID IS NOT NULL AND NowcomAccountStatus = 'Active' AND P.LastDealDate IS NOT NULL) THEN 0 
												WHEN(A.SalesRepID IS NOT NULL AND NowcomAccountStatus = 'Active') THEN 1
												WHEN(NowcomAccountStatus = 'Active' AND P.LastDealDate IS NOT NULL) THEN 2
												WHEN(NowcomAccountStatus = 'Active') THEN 3
												WHEN(A.SalesRepID IS NOT NULL AND P.LastDealDate IS NOT NULL AND NowcomAccountStatus IS NOT NULL AND NowcomAccountStatus <> 'Active') THEN 4
												WHEN(A.SalesRepID IS NOT NULL AND NowcomAccountStatus IS NOT NULL AND NowcomAccountStatus <> 'Active') THEN 5
												WHEN(A.SalesRepID IS NOT NULL AND P.LastDealDate IS NOT NULL) THEN 6
												WHEN(A.SalesRepID IS NOT NULL) THEN 7
												ELSE 99 END, ISNULL(P.LastDealDate, '1/1/1900') DESC) AS Priority,
			A.AccountID, T.ParentAccountID, A.DealerCode, A.AccountName /* DealerDBA ?*/ AS DealerName, A.RegionID, 
			ISNULL(C.FirstName + ' ', '') + ISNULL(C.LastName + ' - ', '') + ISNULL(J.Name, '') AS PrimaryName,
			ISNULL(C.Phone, '') AS PrimaryPhone, A.SalesRepID AS AssignedRepID, U.FullName AS AssignedRep, A.QueuedRepID,
			P.LastDealDate, ADDR.State AS DealerState, ADDR.Longitude, ADDR.Latitude, 
			'http://www.google.com/gmm/x?action=ROUT&end=' + REPLACE(ISNULL(ADDR.Address1, ''), ' ', '%20') 
				+ CASE WHEN (ADDR.Address2 IS NOT NULL) THEN '%20' + REPLACE(ADDR.Address2, ' ', '%20') ELSE '' END
				+ ',%20' + REPLACE(ISNULL(ADDR.City, ''), ' ', '%20') + ',%20' + REPLACE(ISNULL(ADDR.State, ''), ' ', '%20') + 
				'%20' + REPLACE(ISNULL(ADDR.Zip, ''), ' ', '%20') AS BlackberryLink, 
			N.NowcomAccountStatus, QC.DaysSinceLastQueued, QC.QueuedCount						
	--INTO	#DarkDealers -- drop table #DarkDealers
	FROM	#Account T
			INNER JOIN Account.Accounts A ON T.AccountID = A.AccountID 
			LEFT OUTER JOIN Account.Address ACADR ON A.AccountID = ACADR.AccountID AND AddressTypeID = 3 /* 3: Dealer */
			LEFT OUTER JOIN Address.Address ADDR ON ACADR.AddressID = ADDR.AddressID	
			LEFT OUTER JOIN Account.NowcomInformation N ON T.AccountID = N.AccountID 
			LEFT OUTER JOIN Security.Users U ON A.SalesRepID = U.UserID
			LEFT OUTER JOIN Account.Performance P ON A.AccountID = P.AccountID
			LEFT OUTER JOIN Account.Contacts C ON A.AccountID = C.AccountID AND IsPrimaryContact = 1 AND IsEnabled = 1
			LEFT OUTER JOIN Lookup.JobTitle J ON C.JobTitleID = J.JobTitleID			
			LEFT OUTER JOIN		-- Get the number of Queued Rep Assignment since going dark
			(	
				SELECT	ISNULL(P.AccountID, L.AccountID) AS AccountID, 
						SUM(CASE WHEN L.QueuedRepAssignedDate > ISNULL(P.LastDealDate, '01/01/1900') THEN 1 ELSE 0 END) QueuedCount, 
						DATEDIFF(DAY, MAX(QueuedRepAssignedDate), GETDATE()) AS DaysSinceLastQueued
				FROM	Account.Performance P
						LEFT OUTER JOIN #HistoricalRepAssignment L ON P.AccountID = L.AccountID
				GROUP BY ISNULL(P.AccountID, L.AccountID)
			) QC ON T.AccountID = QC.AccountID
	WHERE ( ( ActivityCountForSalesRep IS NULL AND NoteCountForSalesRep IS NULL AND A.QueuedRepID IS NULL)
				OR ( ActivityCountForQueuedRep IS NULL AND NoteCountForQueuedRep IS NULL  AND A.QueuedRepID IS NOT NULL) )
			AND A.AccountID NOT IN 
			(	
				SELECT DISTINCT ParentAccountID 
				FROM	#Account 
				WHERE	ParentAccountID IS NOT NULL 
						AND NOT (   ( ActivityCountForSalesRep IS NULL AND NoteCountForSalesRep IS NULL AND A.QueuedRepID IS NULL)
									OR ( ActivityCountForQueuedRep IS NULL AND NoteCountForQueuedRep IS NULL  AND A.QueuedRepID IS NOT NULL) )
			)
			AND ISNULL(QC.QueuedCount, 0) < @TimesToQueueDealer
			AND ISNULL(DaysSinceLastQueued, 999) > @DaysSinceLastQueued
			AND NOT EXISTS ( SELECT TOP 1 1 FROM Deal.GeneralInfo WHERE DealStatusID = 2 AND AccountID = T.AccountID ) /* No Pending Deal : DealStatusID =2(Pending) */
			AND T.ParentAccountID IS NULL
	OPTION (MAXDOP 1)


go
