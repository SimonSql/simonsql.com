use study

if OBJECT_ID('sm_test_CL') is not null
	drop table sm_test_CL

create table sm_test_CL
(i int identity(1,1)
,c1 int
,c2 int
)
go

insert into sm_test_CL (c1,c2)
select t.rowno+1, t.rowno+2
  from 
		(
		select ROW_NUMBER() over (order by getdate()) as rowno
		  from sys.all_columns a 
		 cross join sys.all_columns b
		) t
where rowno<=1000000
go

create clustered index CL on sm_test_CL (i) with(pad_index=on, fillfactor=100)
create nonclustered index nc01 on sm_test_CL (c1) with(pad_index=on, fillfactor=100)


set statistics io on

--<Goal>
select * from sm_test_CL
where c1>1000 and c1<2000

---1. Find Max(i), Min(i)------------------------------
select MAX(i), MIN(i) from sm_test_CL 
where c1>1000 and c1<2000


select top 1 i from sm_test_CL
where c1>1000
order by c1 asc

select top 1 i from sm_test_CL
where c1<2000
order by c1 desc

set statistics io on
declare @min_i int, @max_i int
select top 1 @min_i=i from sm_test_CL
where c1>1000
order by c1 asc

select top 1 @max_i=i from sm_test_CL
where c1<2000
order by c1 desc
go

/* Compare for big size of data join */
set statistics io on
declare @min_i int, @max_i int
select top 1 @min_i=i from sm_test_CL
where c1>1000
order by c1 asc

select top 1 @max_i=i from sm_test_CL
where c1<200000
order by c1 desc

select * from sm_test_CL
where i between @min_i and @max_i

------- Noncluseter index seek
select * from sm_test_CL with(index(nc01))		-- Force index for Nested loop join and clustered index seek.
where c1>1000 and c1<200000



/*compare c1 between 1000 and 20000

select * from sm_test_CL
where c1>1000 and c1<20000


declare @min_i int, @max_i int
select top 1 @min_i=i from sm_test_CL
where c1>1000
order by c1 asc

select top 1 @max_i=i from sm_test_CL
where c1<20000
order by c1 desc

select * from sm_test_CL
where i between @min_i and @max_i

*/

--select * from sm_test_CL
--where i between 
--				(select top 1 i 
--				   from sm_test_CL
--				  where c1>1000
--				  order by c1 asc)
--	   and 
--				(select top 1 i 
--				   from sm_test_CL
--				  where c1<20000
--				  order by c1 desc)

--select a.*
--  from sm_test_CL a
--  join (select MAX(i) as max_i, MIN(i) as min_i
--		  from sm_test_CL 
--		 where c1>1000 and c1<2000
--	   ) b
--	on a.i between b.min_i and b.max_i


--select * 
--  from sys.syscacheobjects
-- where objtype = 'Adhoc'
--   and sql like '%select a.*%'


--select st.text, qp.query_plan, cp.cacheobjtype, cp.objtype, cp.plan_handle
--  from sys.syscacheobjects c
--  join sys.dm_exec_cached_plans cp on c.bucketid = cp.bucketid
--cross apply sys.dm_exec_sql_text(cp.plan_handle) st
--cross apply sys.dm_exec_query_plan(cp.plan_handle) qp
-- where c.objtype = 'Adhoc'
--   and c.sql like '%select a.*%'

--dbcc SHOW_STATISTICS(sm_test_CL, CL)
--dbcc SHOW_STATISTICS(sm_test_CL, nc01)


--dbcc help('SHOW_STATISTICS')
--STAT_HEADER | DENSITY_VECTOR | HISTOGRAM


--select top 1 i from sm_test_CL order by c1
--select top 1 i from sm_test_CL order by c1 desc
