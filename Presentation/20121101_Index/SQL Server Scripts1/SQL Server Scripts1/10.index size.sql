use study

sp_spaceused 'sm_test_CL'

sp_helptext sp_spaceused

/************************
1.sp_spaceused
*************************/

--SELECT * FROM sys.dm_db_partition_stats WHERE object_id = OBJECT_ID('dbo.sm_test_CL')

SELECT index_id, reservedpages = SUM (reserved_page_count)
	 , usedpages = SUM (used_page_count)
	 , sum(in_row_used_page_count) --This count includes nonleaf B-tree pages, IAM pages, and all pages included in the in_row_data_page_count column.
	 -- + sum(lob_used_page_count) --text, ntext, image, varchar(max), nvarchar(max), varbinary(max)
	 -- + sum(row_overflow_used_page_count) -- varchar, nvarchar, varbinary, sql_variant, IAM pages are included.
	 , pages = SUM(CASE WHEN (index_id < 2) 
						THEN (in_row_data_page_count + lob_used_page_count + row_overflow_used_page_count)  
						ELSE lob_used_page_count + row_overflow_used_page_count  
				  END)
	 , rCount = SUM(CASE WHEN (index_id < 2) THEN row_count ELSE 0 END)
  FROM sys.dm_db_partition_stats  
 WHERE object_id = OBJECT_ID('dbo.sm_test_CL')
 group by index_id
 

SELECT reservedpages = SUM (reserved_page_count)
	 , usedpages = SUM (used_page_count)
	 , sum(in_row_used_page_count) --This count includes nonleaf B-tree pages, IAM pages, and all pages included in the in_row_data_page_count column.
	 -- + sum(lob_used_page_count) --text, ntext, image, varchar(max), nvarchar(max), varbinary(max)
	 -- + sum(row_overflow_used_page_count) -- varchar, nvarchar, varbinary, sql_variant, IAM pages are included.
	 , pages = SUM(CASE WHEN (index_id < 2) 
						THEN (in_row_data_page_count + lob_used_page_count + row_overflow_used_page_count)  
						ELSE lob_used_page_count + row_overflow_used_page_count  
				  END)
	 , rCount = SUM(CASE WHEN (index_id < 2) THEN row_count ELSE 0 END)
  FROM sys.dm_db_partition_stats  
 WHERE object_id = OBJECT_ID('dbo.sm_test_uCL')
 group by index_id
 
/************************
2.allocation_units
*************************/
SELECT
OBJECT_NAME(i.OBJECT_ID) AS TableName,
i.name AS IndexName,
i.index_id AS IndexID,
SUM(a.used_pages) as Usedpages
FROM sys.indexes AS i
JOIN sys.partitions AS p ON p.OBJECT_ID = i.OBJECT_ID AND p.index_id = i.index_id
JOIN sys.allocation_units AS a ON a.container_id = p.partition_id
where i.object_id=OBJECT_ID('dbo.sm_test_CL')
GROUP BY i.OBJECT_ID,i.index_id,i.name
ORDER BY OBJECT_NAME(i.OBJECT_ID),i.index_id

/************************
3.EXTENTINFO
*************************/
--dbcc EXTENTINFO ('study','sm_test_CL',1) WITH NO_INFOMSGS

--dbcc traceon(2588)

--dbcc help('EXTENTINFO')

declare @tbl TABLE 
(FILE_ID BIGINT
,PAGE_ID BIGINT
,PG_ALLOC BIGINT
,EXT_SIZE BIGINT
,OBJECT_ID BIGINT
,INDEX_ID BIGINT
,PARTITION_NUMBER BIGINT
,PARTITION_ID BIGINT
,IAM_CHAIN_TYPE VARCHAR(1000)
,PFS_BYTE BINARY(8)
)
insert @tbl
exec ('dbcc EXTENTINFO (''study'',''sm_test_CL'',1)')
insert @tbl
exec ('dbcc EXTENTINFO (''study'',''sm_test_CL'',2)')


select object_id, index_id, SUM(pg_alloc) as Usedpages from @tbl
group by OBJECT_ID, index_id
  
  
/************************
4.dm_db_index_physical_stats
*************************/
select index_id, convert(numeric(10,2),sum((page_count*avg_page_space_used_in_percent/100))) used_page_size
, SUM(page_count) as Usedpages
  from (
select *
  from sys.dm_db_index_physical_stats (DB_ID(), object_id('sm_test_CL'), 1 , NULL, 'DETAILED')
 union all
select *
  from sys.dm_db_index_physical_stats (DB_ID(), object_id('sm_test_CL'), 2 , NULL, 'DETAILED')

     ) a
 group by index_id
 order by index_id


/************************
5.DBCC IND(db, tbl, index)
*************************/
declare @tbl2 TABLE
(
PageFID TINYINT,
PagePID INT,
IAMFID TINYINT,
IAMPID INT,
ObjectID INT,
IndexID TINYINT,
PartitionNumber TINYINT,
PartitionID BIGINT,
iam_chain_type VARCHAR(30),
PageType TINYINT,
IndexLevel TINYINT,
NextPageFID TINYINT,
NextPagePID INT,
PrevPageFID TINYINT,
PrevPagePID INT
)
insert into @tbl2
exec ('DBCC IND(''study'',''sm_test_CL'',1)')

insert into @tbl2
exec ('DBCC IND(''study'',''sm_test_CL'',2)')

select indexID
	 , Usedpages=SUM(PageFID) -- all page type
	 , data_page=SUM(case when PageType in (1) then PageFID else 0 end)  --data page only
	 , index_page=SUM(case when PageType in (2) then PageFID else 0 end)  --index page
	 , text_page=SUM(case when PageType in (3,4) then PageFID else 0 end)  --text page
	 , GAM_page=SUM(case when PageType in (8) then PageFID else 0 end)  --GAM
	 , SGAM_page=SUM(case when PageType in (9) then PageFID else 0 end)  --SGAM page
	 , IAM_page=SUM(case when PageType in (10) then PageFID else 0 end)  --IAM
	 , PFS_page=SUM(case when PageType in (11) then PageFID else 0 end)  --PFS
  from @tbl2
 group by indexID
 order by indexID

/*
update sm_test_CL
   set i=i-100
 where c1<100000
 
delete from sm_test_CL
where c1%10=1
*/