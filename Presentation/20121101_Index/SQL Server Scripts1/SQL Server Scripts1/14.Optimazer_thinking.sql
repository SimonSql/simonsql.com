use study
go

if object_id('sm_test_OP') is not null
	drop table sm_test_OP
create table sm_test_OP
(
idx int identity(1,1)
, col1 varchar(255)
, col2 int
)

--create unique clustered index cl_idx on sm_test_OP(idx)
create index nc01_col1 on sm_test_op(col1)

--truncate table sm_test_OP

/*
Run this query several times until not doing table scan.
*/
insert into sm_test_OP
select newid(), 1
go 100

--Test
select * 
  from sm_test_OP
 where col1 = '08D6CCDD-3845-4F46-81DC-292C5619861A'
 option(recompile)

select * 
  from sm_test_OP with(index(0))  --force table scan
 where col1 = '08D6CCDD-3845-4F46-81DC-292C5619861A'
 option(recompile)




 /***************************************************************************************/
--update statistics sm_test_OP with fullscan
--alter index nc01_col1 on sm_test_op rebuild

--dbcc FREEPROCCACHE
--SELECT plan_handle, st.text
--  FROM sys.dm_exec_cached_plans 
--  CROSS APPLY sys.dm_exec_sql_text(plan_handle) AS st
--  WHERE text LIKE N'%from sm_test_OP%';
--  GO


--insert into sm_test_OP
--select top 100 newid(), row_number() over (order by getdate()) 
--  from sys.syscolumns a
--  cross join sys.syscolumns b
--  cross join sys.syscolumns c