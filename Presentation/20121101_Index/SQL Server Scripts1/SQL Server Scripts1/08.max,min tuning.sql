use study

if OBJECT_ID('sm_test_CL') is not null
	drop table sm_test_CL

create table sm_test_CL
(i int identity(1,1)
,c1 int
,c2 int
)
go

insert into sm_test_CL (c1,c2)
select t.rowno+1, t.rowno+2
  from 
		(
		select ROW_NUMBER() over (order by getdate()) as rowno
		  from sys.all_columns a 
		 cross join sys.all_columns b
		) t
where rowno<=1000000
go

create clustered index CL on sm_test_CL (i) with(pad_index=on, fillfactor=100)
create nonclustered index nc01 on sm_test_CL (c1) with(pad_index=on, fillfactor=100)


set statistics io on
--set forceplan off
--set statistics  profile on 
select MAX(i), MIN(i) from sm_test_CL 
where i<1000

--select MAX(i), MIN(i) from sm_test_CL 
--where i<100000



select top 1 i from sm_test_CL 
 where i<1000
 order by i
select top 1 i from sm_test_CL 
 where i<1000
 order by i desc

/*
Logical query processing step numbers

(8)  SELECT (9) DISTINCT (11) <TOP_specification> <select_list>
(1)  FROM <left_table>
(3)    <join_type> JOIN <right_table>
(2)      ON <join_condition>
(4)  WHERE <where_condition>
(5)  GROUP BY <group_by_list>
(6)  WITH {CUBE | ROLLUP}
(7)  HAVING <having_condition>
(10) ORDER BY <order_by_list>

1.  FROM: A Cartesian product (cross join) is performed between the first two tables in the FROM clause, and as a result, virtual table VT1 is generated.
2.  ON: The ON filter is applied to VT1. Only rows for which the <join_condition> is TRUE are inserted to VT2.
3.  OUTER (join): If an OUTER JOIN is specified (as opposed to a CROSS JOIN or an INNER JOIN), rows from the preserved table or tables for which a match was not found are added to the rows from VT2 as outer rows, generating VT3. If more than two tables appear in the FROM clause, steps 1 through 3 are applied repeatedly between the result of the last join and the next table in the FROM clause until all tables are processed.
4.  WHERE: The WHERE filter is applied to VT3. Only rows for which the <where_condition> is TRUE are inserted to VT4.
5.  GROUP BY: The rows from VT4 are arranged in groups based on the column list specified in the GROUP BY clause. VT5 is generated.
6.  CUBE | ROLLUP: Supergroups (groups of groups) are added to the rows from VT5, generating VT6.
7.  HAVING: The HAVING filter is applied to VT6. Only groups for which the <having_condition> is TRUE are inserted to VT7.
8.  SELECT: The SELECT list is processed, generating VT8.
9.  DISTINCT: Duplicate rows are removed from VT8. VT9 is generated.
10.  ORDER BY: The rows from VT9 are sorted according to the column list specified in the ORDER BY clause. A cursor is generated (VC10).
11.  TOP: The specified number or percentage of rows is selected from the beginning of VC10. Table VT11 is generated and returned to the caller.
*/
