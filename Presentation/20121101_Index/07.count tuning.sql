use study


if OBJECT_ID('sm_test_CL') is not null
	drop table sm_test_CL

create table sm_test_CL
(i int identity(1,1)
,c1 int
,c2 int
)
go

insert into sm_test_CL (c1,c2)
select t.rowno+1, t.rowno+2
  from 
		(
		select ROW_NUMBER() over (order by getdate()) as rowno
		  from sys.all_columns a 
		 cross join sys.all_columns b
		) t
where rowno<=100000
go

create clustered index CL on sm_test_CL (i) with(pad_index=on, fillfactor=100)

set statistics io on
select * from sm_test_CL
select COUNT(*) from sm_test_CL


create nonclustered index nc01 on sm_test_CL (i) with(pad_index=on, fillfactor=100)
select * from sm_test_CL
select COUNT(*) from sm_test_CL


select COUNT(*) from sm_test_CL
select COUNT(i) from sm_test_CL
select COUNT(1) from sm_test_CL
select COUNT(c1) from sm_test_CL
