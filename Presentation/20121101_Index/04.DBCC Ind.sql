dbcc ind ( { 'dbname' | dbid }, { 'objname' | objid }, { nonclustered indid | 1 | 0 | -1 | -2 } [, partition_number] )
DBCC IND
(
['database name'|database id], -- the database to use
table name, -- the table name to list results
index id, -- an index_id from sys.indexes; -1 shows all indexes and IAMs, -2 just show IAMs
)

PageFID - the file ID of the page
PagePID - the page number in the file
IAMFID - the file ID of the IAM page 
		that maps this page (this will be NULL for IAM pages themselves as they''re not self-referential)
IAMPID - the page number in the file of the IAM page that maps this page
ObjectID - the ID of the object this page is part of
IndexID - the ID of the index this page is part of
PartitionNumber - the partition number (as defined by the partitioning scheme for the index) 
					of the partition this page is part of
PartitionID - the internal ID of the partition this page is part of
iam_chain_type - see IAM chains and allocation units in SQL Server 2005
PageType - the page type. Some common ones are:
      o1 - data page
      o2 - index page
      o3 and 4 - text pages
      o8 - GAM page
      o9 - SGAM page
      o10 - IAM page
      o11 - PFS page
IndexLevel - what level the page is at in the index (if at all). 
			Remember that index levels go from 0 at the leaf to N at the root page 
			(except in clustered indexes in SQL Server 2000 and 7.0 
			- where there''s a 0 at the leaf level (data pages) and a 0 at the next level up (first level of index pages))
NextPageFID and NextPagePID - the page ID of the next page in the doubly-linked list of pages at this level of the index
PrevPageFID and PrevPagePID - the page ID of the previous page in the doubly-linked list of pages at this level of the index
