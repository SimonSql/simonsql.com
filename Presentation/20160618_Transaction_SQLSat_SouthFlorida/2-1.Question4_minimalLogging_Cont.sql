USE Demo
GO
/*******************************************************
Question 4. Solution
*******************************************************/
ALTER DATABASE Demo SET RECOVERY SIMPLE
go
TRUNCATE TABLE Tbl
GO
DBCC DROPCLEANBUFFERS
GO
CHECKPOINT
SET STATISTICS IO ON
SET STATISTICS TIME ON

PRINT '---------Simple Recovery Start--------'
INSERT INTO Tbl
SELECT TOP 100000 'A'
  FROM sys.objects a
  CROSS JOIN sys.objects b
  CROSS JOIN sys.objects c
  CROSS JOIN sys.objects d
  CROSS JOIN sys.objects e
  CROSS JOIN sys.objects f
PRINT '---------Simple Recovery End--------'
SET STATISTICS IO OFF
SET STATISTICS TIME OFF
GO
SELECT operation, context, [log record fixed length], [log record length], AllocUnitId, AllocUnitName
 FROM fn_dblog(null, null)
 WHERE allocunitname='dbo.Tbl'
 ORDER BY [Log Record Length] DESC;
GO

ALTER DATABASE Demo SET RECOVERY BULK_LOGGED
go
BACKUP DATABASE Demo to disk='Nul'
go
BACKUP log Demo to disk='Nul'
go
TRUNCATE TABLE Tbl
GO
DBCC DROPCLEANBUFFERS
GO
CHECKPOINT
SET STATISTICS IO ON
SET STATISTICS TIME ON

PRINT '---------BULK_LOGGED Recovery Start--------'
INSERT INTO Tbl
SELECT TOP 100000 'A'
  FROM sys.objects a
  CROSS JOIN sys.objects b
  CROSS JOIN sys.objects c
  CROSS JOIN sys.objects d
  CROSS JOIN sys.objects e
  CROSS JOIN sys.objects f
PRINT '---------BULK_LOGGED Recovery End--------'
SET STATISTICS IO OFF
SET STATISTICS TIME OFF
GO
SELECT operation, context, [log record fixed length], [log record length], AllocUnitId, AllocUnitName
 FROM fn_dblog(null, null)
 WHERE allocunitname='dbo.Tbl'
 ORDER BY [Log Record Length] DESC;
GO


ALTER DATABASE Demo SET RECOVERY FULL
go
BACKUP DATABASE Demo to disk='Nul'
go
BACKUP log Demo to disk='Nul'
go
TRUNCATE TABLE Tbl
GO
DBCC DROPCLEANBUFFERS
GO
CHECKPOINT
SET STATISTICS IO ON
SET STATISTICS TIME ON
PRINT '---------FULL Recovery Start--------'
INSERT INTO Tbl
SELECT TOP 100000 'A'
  FROM sys.objects a
  CROSS JOIN sys.objects b
  CROSS JOIN sys.objects c
  CROSS JOIN sys.objects d
  CROSS JOIN sys.objects e
  CROSS JOIN sys.objects f
PRINT '---------FULL Recovery End--------'
SET STATISTICS IO OFF
SET STATISTICS TIME OFF
GO
SELECT operation, context, [log record fixed length], [log record length], AllocUnitId, AllocUnitName
 FROM fn_dblog(null, null)
 WHERE allocunitname='dbo.Tbl'
 ORDER BY [Log Record Length] DESC;
GO

