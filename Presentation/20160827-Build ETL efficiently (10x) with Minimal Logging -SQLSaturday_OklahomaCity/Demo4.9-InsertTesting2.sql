USE [master]
GO
EXECUTE AS LOGIN='sa'
go
CREATE DATABASE [Simon_Target] ON 
( FILENAME = N'D:\SQLData\Simon_Target.mdf' ),
( FILENAME = N'D:\SQLData\Simon_Target_log.ldf' )
 FOR ATTACH
GO

USE [Simon_Target]
GO
ALTER DATABASE [Simon_Target] SET RECOVERY BULK_LOGGED 
GO
BACKUP DATABASE [Simon_Target] TO DISK='Nul' WITH stats=1
GO

/***********************
Testing 1 : Bulk logged.
***********************/
TRUNCATE TABLE [dbo].[S1_SimonTest_Target]
GO
BACKUP LOG [Simon_Target] TO DISK='Nul' WITH stats=1
GO
CHECKPOINT
GO
SELECT * FROM sys.fn_dblog(NULL,null)


-- 10 Sec-12 Sec
INSERT INTO [Simon_Target].[dbo].[S1_SimonTest_Target] WITH(TABLOCK)
SELECT * FROM [SELF].Simon_Source.[dbo].[S1_SimonTest]

SELECT COUNT(*), SUM([Log Record Length]) FROM sys.fn_dblog(NULL,null)
--19246, 1176004


/***********************
Testing 2 : Full recovery
***********************/
ALTER DATABASE [Simon_Target] SET RECOVERY FULL
GO
TRUNCATE TABLE [dbo].[S1_SimonTest_Target]
GO
BACKUP LOG [Simon_Target] TO DISK='Nul' WITH stats=1
GO
CHECKPOINT
GO
SELECT COUNT(*), SUM([Log Record Length]) FROM sys.fn_dblog(NULL,null)


-- 12 Sec-15 Sec
INSERT INTO [Simon_Target].[dbo].[S1_SimonTest_Target] WITH(TABLOCK)
SELECT * FROM [SELF].Simon_Source.[dbo].[S1_SimonTest]

SELECT COUNT(*), SUM([Log Record Length]) FROM sys.fn_dblog(NULL,null)
--29122,95071212



/***********************
Testing 2 : Full recovery
***********************/
ALTER DATABASE [Simon_Target] SET RECOVERY FULL
GO
TRUNCATE TABLE [dbo].[S1_SimonTest_Target]
GO
BACKUP LOG [Simon_Target] TO DISK='Nul' WITH stats=1
GO
CHECKPOINT
GO
SELECT COUNT(*), SUM([Log Record Length]) FROM sys.fn_dblog(NULL,null)


-- 12 Sec-15 Sec
INSERT INTO [Simon_Target].[dbo].[S1_SimonTest_Target] WITH(TABLOCK)
SELECT * FROM [SELF].Simon_Source.[dbo].[S1_SimonTest]

SELECT COUNT(*), SUM([Log Record Length]) FROM sys.fn_dblog(NULL,null)
--29122,95071212
