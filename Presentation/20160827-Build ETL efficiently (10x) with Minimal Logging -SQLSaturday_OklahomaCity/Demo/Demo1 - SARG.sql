USE Simon_source

--Query1
DECLARE @BaseAccountID BIGINT = 1
SELECT *
  FROM DBO.[Accounts]
 WHERE AccountID = @BaseAccountID + 1000
GO

--Query2
DECLARE @BaseAccountID BIGINT = 1
SELECT *
  FROM DBO.[Accounts]
 WHERE AccountID - 1000 = @BaseAccountID
GO

--Query1
SELECT AccountID, AccountName
  FROM DBO.[Accounts]
 WHERE AccountID = 1000

SELECT AccountID, AccountName
  FROM DBO.[Accounts]
 WHERE AccountID = '1000'
GO

--Query2
SELECT AccountID, AccountName
  FROM DBO.[Accounts]
 WHERE AccountName = 1000

SELECT AccountID, AccountName
  FROM DBO.[Accounts]
 WHERE AccountName = '1000'
GO


--Query1
SELECT *
  FROM DBO.[Accounts]
 WHERE Registered_Datetime > '2016-02-11'

--Query2
DECLARE @Registered_datetime DATETIME = '2016-02-11'
SELECT *
  FROM DBO.[Accounts]
 WHERE Registered_Datetime > @Registered_datetime
GO
