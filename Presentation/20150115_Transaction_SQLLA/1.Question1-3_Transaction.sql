USE Demo
GO
CHECKPOINT

/*******************************************************
1.Question 1.
*******************************************************/
BEGIN TRAN A
	INSERT [Tbl] values('A')
	BEGIN TRAN B
		INSERT [Tbl] values('B')
	ROLLBACK TRAN B
COMMIT TRAN A
GO





IF @@TRANCOUNT>0
	ROLLBACK TRAN
CHECKPOINT
--Cntr-9
GO

/*******************************************************
2.Question 2.
*******************************************************/
BEGIN TRAN AAAA
	INSERT [Tbl] values('A')
	BEGIN TRAN BBBB
		INSERT [Tbl] values('B')
	COMMIT TRAN AAAA
COMMIT TRAN BBBB



IF @@TRANCOUNT>0
	ROLLBACK TRAN
CHECKPOINT
--Cntr-9

/*******************************************************
2.Question 2-1
*******************************************************/
-- Even totally different name.
BEGIN TRAN AAAA
	INSERT [Tbl] values('A')
	BEGIN TRAN CCCC
		INSERT [Tbl] values('B')
	COMMIT TRAN AAAA
COMMIT TRAN BBBB


IF @@TRANCOUNT>0
	ROLLBACK TRAN
CHECKPOINT
--Cntr-9

/*******************************************************
2.Question 3
*******************************************************/

BEGIN TRAN
	TRUNCATE TABLE [Tbl]
ROLLBACK TRAN


/*******************************************************
2.Question 3 - CONT
*******************************************************/
INSERT INTO Tbl
SELECT TOP 10000 'A'
  FROM SYS.OBJECTS AS T1 
 CROSS JOIN SYS.OBJECTS AS T2

CHECKPOINT

BEGIN TRANSACTION 
	DELETE FROM Tbl
	--Cntr-9
ROLLBACK TRANSACTION

CHECKPOINT
BEGIN TRANSACTION 
	TRUNCATE TABLE Tbl
	--Cntr-9
ROLLBACK TRANSACTION

 