/*******************************************************
0.Initial Setup
*******************************************************/
USE master
GO

IF DB_ID('Demo') IS NOT NULL
	ALTER DATABASE Demo SET SINGLE_USER WITH ROLLBACK IMMEDIATE
	DROP DATABASE Demo
GO

CREATE DATABASE Demo
GO

ALTER DATABASE Demo SET RECOVERY SIMPLE
GO

USE Demo

IF OBJECT_ID('Tbl') IS NOT NULL
	DROP TABLE Tbl
GO

CREATE TABLE [Tbl]
(
Col1 VARCHAR(255)
)

CHECKPOINT

SELECT [Current LSN]
	 , Operation, Context
	 , [Log Record Fixed Length]
	 , [Log Record Length]
	 , AllocUnitName
	 , [Page ID]
	 , [Checkpoint Begin]
	 , [Checkpoint Begin]
	 , Description
	 , [Xact ID] 
  FROM fn_dblog(null, null)

-- Created Keyboard short-cut for this query
--Cntr-0
--SELECT * FROM 
--Cntr-9
--SELECT [Current LSN], Operation, Context, [Log Record Fixed Length], [Log Record Length], AllocUnitName, [Page ID], [Checkpoint Begin], [Checkpoint Begin], Description, [Xact ID]  FROM fn_dblog(null, null)
--Close and open again SSMS