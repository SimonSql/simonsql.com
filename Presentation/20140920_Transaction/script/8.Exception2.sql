USE demo
GO

IF OBJECT_ID('dbo.simple_tbl') IS NULL
	CREATE TABLE simple_tbl
	(CategoryId INT PRIMARY KEY
	, RegisterDatetime DATETIME)
GO

/*
case 3: simultaneously 
*/

DECLARE @intCategoryID INT
SET @intCategoryID = 1

IF NOT EXISTS (SELECT * FROM dbo.simple_tbl WHERE Categoryid = @intCategoryID) BEGIN
    INSERT dbo.simple_tbl (CategoryId, RegisterDatetime)
	VALUES (@intCategoryID, GETDATE())
	SELECT 'Inserted' AS RESULT
END ELSE BEGIN
	SELECT 'Not_Inserted' AS RESULT
END

INSERT INTO dbo.simple_tbl (CategoryId, RegisterDatetime)
SELECT @intCategoryID, GETDATE()
  FROM dbo.simple_tbl
 WHERE Categoryid = 1



BEGIN TRANSACTION
	INSERT INTO simple_tbl (CategoryId, RegisterDatetime) VALUES (1,getdate())
	INSERT INTO simple_tbl (CategoryId, RegTime) VALUES (2,getdate())
	INSERT INTO simple_tbls (CategoryId, RegisterDatetime) VALUES (3,getdate())
COMMIT TRANSACTION


/*
case 3: XACT_ABORT
*/

CREATE PROC TestTran
as
Declare @Err INT
SET XACT_ABORT OFF
BEGIN TRANSACTION
BEGIN TRY
	SELECT * FROM NoTable
	COMMIT TRAN
END TRY
BEGIN CATCH
	
	SET @ERR = @@ERROR
	IF @Err <> 0 BEGIN
		RAISERROR('Encountered an error, rollback', 16,1)
		IF @@TRANCOUNT <> 0
			ROLLBACK
		RETURN (@ERR)
	END
END CATCH
GO

EXEC TestTran

select @@TRANCOUNT
ROLLBACK TRAN