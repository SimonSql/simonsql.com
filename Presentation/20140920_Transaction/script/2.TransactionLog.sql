use study
go
/*
Case1 compare insert 1000 record In/NotIn a transaction
*/
ALTER DATABASE STUDY SET RECOVERY SIMPLE

IF OBJECT_ID('BiginEnd') IS NOT NULL
	DROP TABLE BiginEnd

CREATE TABLE BiginEnd
(
idx INT
, col char(10)
)

CHECKPOINT

INSERT INTO BiginEnd VALUES (1,'A')
GO 10

SELECT * FROM fn_dblog(null, null)

--VS

CHECKPOINT
BEGIN TRANSACTION
INSERT INTO BiginEnd VALUES (1,'A')
INSERT INTO BiginEnd VALUES (1,'A')
INSERT INTO BiginEnd VALUES (1,'A')
INSERT INTO BiginEnd VALUES (1,'A')
INSERT INTO BiginEnd VALUES (1,'A')
INSERT INTO BiginEnd VALUES (1,'A')
INSERT INTO BiginEnd VALUES (1,'A')
INSERT INTO BiginEnd VALUES (1,'A')
INSERT INTO BiginEnd VALUES (1,'A')
INSERT INTO BiginEnd VALUES (1,'A')
COMMIT TRANSACTION

SELECT * FROM fn_dblog(null, null)

TRUNCATE TABLE BiginEnd
CHECKPOINT

/*
Case2 Shutdown server and restore back during the transaction
*/
BEGIN TRANSACTION
INSERT INTO BiginEnd VALUES (1,'A')
GO 100000


CHECKPOINT
SELECT * FROM fn_dblog(null, null)

-- ShutDown with out commit or rollback
SHUTDOWN

EXEC SP_READERRORLOG 0,1, 'STUDY'
SELECT * FROM fn_dblog(null, null)

SELECT @@TRANCOUNT

SELECT COUNT(*) FROM BiginEnd

/*
Case3 Rollback transaction
*/

CHECKPOINT
SELECT * FROM fn_dblog(null, null)


BEGIN TRANSACTION
INSERT INTO BiginEnd VALUES (1,'A')
INSERT INTO BiginEnd VALUES (1,'A')
INSERT INTO BiginEnd VALUES (1,'A')
INSERT INTO BiginEnd VALUES (1,'A')
INSERT INTO BiginEnd VALUES (1,'A')
INSERT INTO BiginEnd VALUES (1,'A')
INSERT INTO BiginEnd VALUES (1,'A')
INSERT INTO BiginEnd VALUES (1,'A')
INSERT INTO BiginEnd VALUES (1,'A')
INSERT INTO BiginEnd VALUES (1,'A')
ROLLBACK TRANSACTION
SELECT * FROM fn_dblog(null, null)

/*
How do checkpoints work and what gets logged
http://www.sqlskills.com/blogs/paul/how-do-checkpoints-work-and-what-gets-logged/
*/

DBCC SQLPERF ('LOGSPACE');
DBCC LOG('STUDY')
