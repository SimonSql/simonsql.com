USE [DocConService]
GO

ALTER PROCEDURE [dbo].[USP_DocumentData_Cleaup]
@RetentionDate INT = 90
,@DeleteRecord INT = 10
AS
/***********************************************************************************************
**Object Name: dbo.USP_DocumentData_Cleaup
**
**Description: Delete DocConService record. 
-- Default 90 days retention policy.
-- Delete 10 records on Document table and related records per one transaction
**
**Input Parameters: 
	@RetentionDate INT = 90
	, @DeleteRecord INT = 10
**
**Return Value: N/A
**
**
**Creator: Simon Cho
**
**Create Date: 1/14/2014
**
**Example: exec USP_DocumentData_Cleaup
**
**Change History
**
**Change Date			Changed By				Reason
*************************************************************************************************/
BEGIN
	SET NOCOUNT ON;
	DECLARE @ErrorMessage NVARCHAR(4000),
			@ErrorNumber INT,
			@ErrorSeverity INT,
			@ErrorState INT,
			@ErrorLine INT,
			@ErrorProcedure NVARCHAR(200);
	
	CREATE TABLE #Delete_DocumentID([DocumentID] uniqueidentifier)

	DECLARE @ROWCOUNT BIGINT

	SET @ROWCOUNT = 1

	WHILE (@ROWCOUNT>0) 
	BEGIN TRY
		BEGIN TRANSACTION
		
		--Delete record based on @DeleteRecord size.
		INSERT INTO #Delete_DocumentID
		SELECT TOP(@DeleteRecord) DocumentID FROM [dbo].[Document]
		 WHERE [DateCreated] < GETDATE()-@RetentionDate

		SET @ROWCOUNT = @@ROWCOUNT
		PRINT 'Document:'+convert(varchar(255), @ROWCOUNT)

		DELETE FROM dbo.DocumentPage
		  FROM dbo.DocumentPage T
		  JOIN #Delete_DocumentID DD
			ON T.DocumentID = DD.DocumentID
		PRINT 'DocumentPage:'+convert(varchar(255), @@ROWCOUNT)

		DELETE FROM dbo.DocumentFile
		  FROM dbo.DocumentFile T
		  JOIN #Delete_DocumentID DD
			ON T.DocumentID = DD.DocumentID
		PRINT 'DocumentFile:'+convert(varchar(255), @@ROWCOUNT)

		DELETE FROM dbo.SourceInfo
		  FROM dbo.SourceInfo T
		  JOIN #Delete_DocumentID DD
			ON T.DocumentID = DD.DocumentID
		PRINT 'SourceInfo:'+convert(varchar(255), @@ROWCOUNT)
 
		DELETE FROM dbo.AuditLog
		  FROM dbo.AuditLog T
		  JOIN #Delete_DocumentID DD
			ON T.DocumentID = DD.DocumentID
		PRINT 'AuditLog:'+convert(varchar(255), @@ROWCOUNT)

		DELETE FROM dbo.Document
		  FROM dbo.Document T
		  JOIN #Delete_DocumentID DD
			ON T.DocumentID = DD.DocumentID
		 
		DELETE FROM #Delete_DocumentID

		COMMIT TRANSACTION
	END TRY
	BEGIN CATCH
		SELECT @ErrorNumber = ERROR_NUMBER()
			 , @ErrorSeverity = ERROR_SEVERITY()
			 , @ErrorState = ERROR_STATE()
			 , @ErrorLine = ERROR_LINE()
			 , @ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-')
			 , @ErrorMessage = ERROR_MESSAGE()
					
		SELECT @ErrorMessage = N'Error %d, Level %d, State %d, Procedure %s, Line %d, ' + 'Message: ' + ISNULL(@ErrorMessage,'unKnown')

		IF @@TRANCOUNT > 0
			ROLLBACK TRANSACTION

		RAISERROR (@ErrorMessage, @ErrorSeverity, @ErrorState, @ErrorNumber, -- parameter: original error number.
			@ErrorSeverity, -- parameter: original error severity.
			@ErrorState, -- parameter: original error state.
			@ErrorProcedure, -- parameter: original error procedure name.
			@ErrorLine-- parameter: original error line number.
		)
	END CATCH

	IF @@TRANCOUNT > 0 BEGIN
		ROLLBACK TRANSACTION
		RAISERROR ('Unknown Error: Transaction Open', 16, 1)
	END
END
