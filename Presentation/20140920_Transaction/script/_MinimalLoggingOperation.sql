http://technet.microsoft.com/en-us/library/ms190422%28v=sql.100%29.aspx
http://simonsql.com/?s=minimal

For a database under the full recovery model, all row-insert operations that are performed by bulk import are fully logged in the transaction log.
Large data imports can cause the transaction log to fill rapidly if the full recovery model is used.
In contrast, under the simple recovery model or bulk-logged recovery model, minimal logging of bulk-import operations reduces the possibility 
that a bulk-import operation will fill the log space. Minimal logging is also more efficient than full logging.

# Table Requirements for Minimally Logging Bulk-Import Operations
- The table is not being replicated.
- Table locking is specified (using TABLOCK).

1. If the table has no indexes, data pages are minimally logged.
2. If the table has no clustered index but has one or more nonclustered indexes, data pages are always minimally logged.
   How index pages are logged, however, depends on whether the table is empty:
	- If the table is empty, index pages are minimally logged.
	- If table is non-empty, index pages are fully logged.
	#note : If you start with an empty table and bulk import the data in multiple batches
			, both index and data pages are minimally logged for the first batch
			, but beginning with the second batch
			, only data pages are minimally logged.

3. If the table has a clustered index and is empty, both data and index pages are minimally logged.
   In contrast, if a table has a clustered index and is non-empty, data pages and index pages are both fully logged regardless of the recovery model.
	- If you start with an empty table and bulk import the data in batches, both index and data pages are minimally logged for the first batch, but from the second batch onwards, only data pages are bulk logged.


example) http://www.sqlservercentral.com/articles/Administration/100856/
-- Set Recover model to BULK_LOGGED
ALTER DATABASE TSQL2012
 SET RECOVERY BULK_LOGGED;

IF OBJECT_ID('TarTable') IS NOT NULL 
 DROP TABLE TarTable;

IF OBJECT_ID('TarHeap') IS NOT NULL 
 DROP TABLE TarHeap;

IF OBJECT_ID('SrcHeap') IS NOT NULL 
 DROP TABLE SrcHeap;

CREATE TABLE TarHeap( col1 INT ,col2 CHAR(4000),col3 CHAR(1000) ) ;

CREATE TABLE SrcHeap (col1 INT ,col2 CHAR(4000),col3 CHAR(1000) ) ; 

CREATE TABLE TarTable (col1 INT PRIMARY KEY ,col2 CHAR(4000),col3 CHAR(1000) ); 

--Insert row into source table
WITH Nums (col)
AS 
(
 SELECT 1 col
 UNION ALL 
 SELECT col + 1 FROM Nums
 WHERE col+1 <= 10000
)
INSERT INTO SrcHeap(col1) 
 SELECT col FROM Nums 
 OPTION (MAXRECURSION 10000)

--Insert rows to Target Table with (TABLOCK) Minimally logged
INSERT INTO TarHeap WITH(TABLOCK)
 SELECT * FROM SrcHeap 

-- Check Log Entries
SELECT TOP 10 operation [MINIMALLY LOGGED OPERATION ],context, [log record fixed length], [log record length], AllocUnitId, AllocUnitName
 FROM fn_dblog(null, null)
 WHERE allocunitname='dbo.TarHeap'
 ORDER BY [Log Record Length] DESC;

--Note That Log Record length is small 

--Insert rows to Target Table without (TABLOCK) fully logged
INSERT INTO TarHeap 
 SELECT * FROM SrcHeap WITH(NOLOCK);

-- Check Log Entries
SELECT TOP 10 operation [FULLY LOGGED OPERATION],context, [log record fixed length], [log record length], AllocUnitId, AllocUnitName
 FROM fn_dblog(null, null)
 WHERE allocunitname='dbo.TarHeap'
 ORDER BY [Log Record Length] DESC;

--Note That Log Record length is big 

--Insert rows to Target Table with clustered index and trace flag off - fully logged
INSERT INTO TarTable 
 SELECT * FROM SrcHeap WITH(NOLOCK);

SELECT TOP 10 operation [FULLY LOGGED OPERATION - EMPTY TABLE WITH CLUST INDEX 610 FLAG OFF],context, [log record fixed length], [log record length], AllocUnitId, AllocUnitName
 FROM fn_dblog(null, null)
 WHERE allocunitname LIKE '%TarTable%'
 ORDER BY [Log Record Length] DESC;

--Note That Log Record length is big 

CHECKPOINT;
GO
DBCC TRACEON(610);
TRUNCATE TABLE TarTable;
GO

--Insert rows to Target Table with clustered index empty table and trace flag ON - Minimally logged
INSERT INTO TarTable WITH(TABLOCK)
 SELECT * FROM SrcHeap WITH(NOLOCK); 

SELECT TOP 10 operation [MINIMALLY LOGGED OPERATION - EMPTY TABLE WITH CLUST INDEX 610 FLAG ON],context, [log record fixed length], [log record length], AllocUnitId, AllocUnitName
 FROM fn_dblog(null, null)
 WHERE allocunitname LIKE '%TarTable%'
 ORDER BY [Log Record Length] DESC;

--Note That Log Record length is small
GO

-- Turn off trace flag
DBCC TRACEOFF(610);

-- Set recovery model back to full
ALTER DATABASE TSQL2012
 SET RECOVERY FULL;