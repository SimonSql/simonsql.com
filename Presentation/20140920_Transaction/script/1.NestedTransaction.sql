USE master
GO

IF DB_ID('Demo') IS NOT NULL
	ALTER DATABASE Demo SET SINGLE_USER WITH ROLLBACK IMMEDIATE
	DROP DATABASE Demo
GO

CREATE DATABASE Demo
GO

USE Demo
GO

IF OBJECT_ID('Tbl') IS NOT NULL
	DROP TABLE Tbl
GO
CHECKPOINT

SELECT [Current LSN], Operation, Context, [Log Record Fixed Length], [Log Record Length], AllocUnitName, [Page ID], [Checkpoint Begin], [Checkpoint Begin], Description, [Xact ID] 
  FROM fn_dblog(null, null)

CREATE TABLE [Tbl]
(
Col1 VARCHAR(255)
)

--Question 1.
BEGIN TRAN A
	INSERT [Tbl] values('A')
	BEGIN TRAN B
		INSERT [Tbl] values('B')
	ROLLBACK TRAN B
COMMIT TRAN A
GO

ROLLBACK TRAN
GO

--Question 2.
BEGIN TRAN AAAA
	INSERT [Tbl] values('A')
	BEGIN TRAN BBBB
		INSERT [Tbl] values('B')
	COMMIT TRAN AAAA
COMMIT TRAN BBBB



--Question 3.
BEGIN TRAN
	TRUNCATE TABLE [Tbl]
ROLLBACK TRAN


--Question 3. Is it possible? what's the @@TRANCOUNT RESULT
BEGIN TRAN A
	BEGIN TRAN BB
		BEGIN TRAN CCC
			SELECT @@TRANCOUNT

		COMMIT TRAN CCC
		SELECT @@TRANCOUNT

	ROLLBACK TRAN BB
	SELECT @@TRANCOUNT
COMMIT TRAN A
SELECT @@TRANCOUNT

--Question 4. Is it possible? what's the @@TRANCOUNT RESULT
BEGIN TRAN A
	BEGIN TRAN BB
		BEGIN TRAN CCC
			SELECT @@TRANCOUNT

		ROLLBACK TRAN
		SELECT @@TRANCOUNT

	ROLLBACK TRAN
	SELECT @@TRANCOUNT
COMMIT TRAN A
SELECT @@TRANCOUNT


-- Question 5. Truncate table can be rollback?

INSERT INTO NestedTran VALUES('A')
GO 1000

checkpoint

SELECT * FROM fn_dblog(null, null)
SELECT COUNT(*) FROM NestedTran 

checkpoint
BEGIN TRANSACTION
TRUNCATE TABLE NestedTran
ROLLBACK TRANSACTION
SELECT * FROM fn_dblog(null, null)

checkpoint
BEGIN TRANSACTION
delete from NestedTran
ROLLBACK TRANSACTION
SELECT * FROM fn_dblog(null, null)


SELECT COUNT(*) FROM NestedTran 

-- Question 6. which database recovery mode is the fastest one for this batch
SET STATISTICS IO ON
SET STATISTICS TIME ON
ALTER DATABASE Demo SET RECOVERY SIMPLE
ALTER DATABASE Demo SET RECOVERY BULK_LOGGED
ALTER DATABASE Demo SET RECOVERY FULL

TRUNCATE TABLE NestedTran
checkpoint

DBCC DROPCLEANBUFFERS

INSERT INTO NestedTran
SELECT TOP 1000000 'A'
  FROM sys.objects a
  CROSS JOIN sys.objects b
  CROSS JOIN sys.objects c
  CROSS JOIN sys.objects d
  CROSS JOIN sys.objects e
  CROSS JOIN sys.objects f


print '--------------------------------'
go
/*
--Simple recovery mode result
SQL Server parse and compile time: 
   CPU time = 15 ms, elapsed time = 37 ms.
Table 'NestedTran'. Scan count 0, logical reads 100173, physical reads 0, read-ahead reads 0, lob logical reads 0, lob physical reads 0, lob read-ahead reads 0.
Table 'sysschobjs'. Scan count 6, logical reads 14, physical reads 0, read-ahead reads 0, lob logical reads 0, lob physical reads 0, lob read-ahead reads 0.

 SQL Server Execution Times:
   CPU time = 125 ms,  elapsed time = 724 ms.

(100000 row(s) affected)

--Bulk recovery mode result
SQL Server parse and compile time: 
   CPU time = 19 ms, elapsed time = 19 ms.
Table 'NestedTran'. Scan count 0, logical reads 100173, physical reads 0, read-ahead reads 0, lob logical reads 0, lob physical reads 0, lob read-ahead reads 0.
Table 'sysschobjs'. Scan count 6, logical reads 14, physical reads 0, read-ahead reads 0, lob logical reads 0, lob physical reads 0, lob read-ahead reads 0.

 SQL Server Execution Times:
   CPU time = 125 ms,  elapsed time = 138 ms.

(100000 row(s) affected)

--Full recovery mode result
SQL Server parse and compile time: 
   CPU time = 17 ms, elapsed time = 17 ms.
Table 'NestedTran'. Scan count 0, logical reads 100173, physical reads 0, read-ahead reads 0, lob logical reads 0, lob physical reads 0, lob read-ahead reads 0.
Table 'sysschobjs'. Scan count 6, logical reads 14, physical reads 0, read-ahead reads 0, lob logical reads 0, lob physical reads 0, lob read-ahead reads 0.

 SQL Server Execution Times:
   CPU time = 141 ms,  elapsed time = 377 ms.

(100000 row(s) affected)
*/

--SELECT * FROM sys.fn_dblog(null,null)

backup database Demo to disk='nul'

TRUNCATE TABLE NestedTran
checkpoint

INSERT INTO NestedTran WITH(TABLOCK)
SELECT TOP 1000000 'A'
  FROM sys.objects a
  CROSS JOIN sys.objects b
  CROSS JOIN sys.objects c
  CROSS JOIN sys.objects d
  CROSS JOIN sys.objects e
  CROSS JOIN sys.objects f

SELECT [Current LSN], Operation, Context, [Log Record Fixed Length], [Log Record Length], AllocUnitName, [Page ID], [Checkpoint Begin], [Checkpoint Begin], Description, [Xact ID] 
  FROM fn_dblog(null, null)

--truncate table NestedTran
--SELECT * FROM sys.fn_dblog(null,null)
--checkpoint

/*
A SQL Server DBA myth a day: (26/30) nested transactions are real
http://www.sqlskills.com/blogs/paul/a-sql-server-dba-myth-a-day-2630-nested-transactions-are-real/

*/


BEGIN TRAN
	TRUNCATE TABLE NestedTran
ROLLBACK TRAN
