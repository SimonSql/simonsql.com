BEGIN TRY
    IF NOT EXISTS (SELECT * FROM dbo.Categories WHERE categoryID = @intCategoryID)
    BEGIN
        SET @intReturnValue = 103;
        GOTO ErrorHandler;
    END
    INSERT dbo.Articles (categoryID, [subject], writer, [content], registerDate)
    VALUES (@intCategoryID, @intSubject, @nvcWriter, @nvcContent, GETDATE());
    SET @intArticleID = SCOPE_IDENTITY();
END TRY
BEGIN CATCH
	RAISERROR('ERROR',16,1)
END CATCH