Use Study
go

-- Data varification in Application is option.
-- Data varification in Database is mentantory.

CREATE TABLE dbo.ErrorLogs(
    errorLogSN        bigint           IDENTITY(1,1),
    loginName         nvarchar(128)    NOT NULL,
    hostName          nvarchar(128)    NOT NULL,
    errorNumber       int              NOT NULL,
    errorSeverity     int              NULL,
    errorState        int              NULL,
    errorProcedure    nvarchar(128)    NULL,
    errorLine         int              NULL,
    errorMessage      nvarchar(max)    NOT NULL,
    occurredDate      datetime         NOT NULL,
    CONSTRAINT PK_ErrorLogs PRIMARY KEY CLUSTERED (errorLogSN)
)
GO

/**
author : Doeyull.Kim
e-mail : purumae@gmail.com
created date : 2008-05-01
description : Log the error that occured.
return value :
0 = There is no error.
1 = The transaction is in an uncommittable state. Rolling back transaction.
100 = System error occurred.
**/

CREATE PROCEDURE dbo.P_AddErrorLog
WITH EXECUTE AS OWNER
AS
BEGIN
	SET NOCOUNT ON;
	SET XACT_ABORT ON;

	DECLARE @intReturnValue int;

	DECLARE
		@nvcErrorMessage nvarchar(4000),
		@intErrorNumber int,
		@intErrorSeverity int,
		@intErrorState int,
		@intErrorLine int,
		@nvcErrorProcedure nvarchar(128);

	/**_# Rollback and return if inside an uncommittable transaction.*/
	IF XACT_STATE() = -1
	BEGIN
		SET @intReturnValue = 1;
		GOTO ErrorHandler;
	END

	/**_# If there is no error information to log, return 0.*/
	IF ERROR_NUMBER() IS NULL
	BEGIN
		SET @intReturnValue = 0;
		GOTO ErrorHandler;
	END

	/**_# Log the error.*/
	/**_## Assign variables to error-handling functions that capture information for RAISERROR.*/
	SET @intErrorNumber = ERROR_NUMBER();
	SET @intErrorSeverity = ERROR_SEVERITY();
	SET @intErrorState = ERROR_STATE();
	SET @intErrorLine = ERROR_LINE();
	SET @nvcErrorProcedure = ERROR_PROCEDURE();
	SET @nvcErrorMessage = ERROR_MESSAGE();

	/**_## Log the error that occurred.*/
	INSERT dbo.ErrorLogs (loginName, hostName, errorNumber, errorSeverity, errorState, errorProcedure, errorLine, errorMessage, occurredDate)
	VALUES (CAST(ORIGINAL_LOGIN() AS nvarchar(128)), CAST(HOST_NAME() AS nvarchar(128)), @intErrorNumber, @intErrorSeverity, @intErrorState, @nvcErrorProcedure, @intErrorLine, @nvcErrorMessage, GETDATE());

	/**_# Rethrow the error.*/
	SET @nvcErrorMessage = N'Error %d, Level %d, State %d, Procedure %s, Line %d, Message: ' + @nvcErrorMessage;
	RAISERROR (@nvcErrorMessage, @intErrorSeverity, 1, @intErrorNumber, @intErrorSeverity, @intErrorState, @nvcErrorProcedure, @intErrorLine);

	RETURN @intErrorNumber;

	ErrorHandler:
		IF XACT_STATE() <> 0
			ROLLBACK TRANSACTION;

	RETURN 0;
END
GO

CREATE PROCEDURE usp_GetErrorInfo
AS
    SELECT 
        ERROR_NUMBER() AS ErrorNumber,
        ERROR_SEVERITY() AS ErrorSeverity,
        ERROR_STATE() as ErrorState,
        ERROR_PROCEDURE() as ErrorProcedure,
        ERROR_LINE() as ErrorLine,
        ERROR_MESSAGE() as ErrorMessage

GO

Create Procedure usp_Naming_Convention
@param int
--, ...
AS
BEGIN
	SET XACT_ABORT ON;
	SET NOCOUNT ON;
	--SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

	DECLARE @ReturnValue INT = 0
	DECLARE @ErrCodeTbl Table(ErrorCode int, Memo varchar(255))
	DECLARE @$ErrorMessage NVARCHAR(4000)
		  , @$ErrorNumber INT
		  , @$ErrorSeverity INT
		  , @$ErrorState INT
		  , @$ErrorLine INT
		  , @$ErrorProcedure NVARCHAR(200)

	DECLARE @$prog VARCHAR(50)
		  , @$proc_section_nm VARCHAR(50)
		  , @$error_db_name VARCHAR(50)

	/*
	Define error number
	50001 : ParameterMissing
	50002 : Data integrity problem...
	...
	0 : Success
	Else : @@Error
	*/

	INSERT INTO @ErrCodeTbl (ErrorCode, Memo)
	SELECT 50001, 'ParameterMissing' UNION ALL
	SELECT 50002, 'Data integrity problem'
			
	--<Vailidation Parameter values>
	--Defalut setting or update null value
	If @param is null BEGIN
		SET @ReturnValue = 50001
		GOTO ERROR
	END
       
	-- additional for non trans proc if a wrapper for transactional
	DECLARE @$prev_trancount INT, @$tran_flag_err INT;
		SET @$prev_trancount = @@TRANCOUNT;

	SELECT @$ErrorNumber = NULL
		 , @$ErrorMessage = NULL
		 , @$prog = LEFT(OBJECT_NAME(@@PROCID),50)
		 , @$error_db_name = db_name();

	IF @$prev_trancount = 0
		BEGIN TRANSACTION

	BEGIN TRY
		-- Step01 : Execute another SP. Get return value
		EXEC @ReturnValue = USP_INNER_TRAN 
		SET @ReturnValue = COALESCE(NULLIF(@ReturnValue, 0), @@ERROR)
		IF @ReturnValue != 0 BEGIN
			GOTO ERROR
		END

		-- Step02
		--Insert/update/delete any user code

		-- Step03
		--Insert/update/delete any user code

		-- Step04
		--Dynamic SQL
		EXEC @ReturnValue = sp_executesql @stmt, @params, @values1, @values2 output
		SET @ReturnValue = COALESCE(NULLIF(@ReturnValue, 0), @@ERROR)

		IF @ReturnValue != 0 BEGIN
			GOTO ERROR
		END	ELSE BEGIN
			GOTO DONE
		END

	END TRY
	BEGIN CATCH
		/*
		UnExpected Error
		*/
		EXEC @ReturnValue = dbo.P_AddErrorLog
		SET @ReturnValue = ISNULL(@ReturnValue, @@ERROR)
		GOTO ERROR
	END CATCH

ERROR:
	/* Case 1 UnExpected Error*****/
		-- Assign variables to error-handling functions that
		-- capture information for RAISERROR.
	SELECT @$ErrorNumber = ERROR_NUMBER()
		 , @$ErrorSeverity = ERROR_SEVERITY()
		 , @$ErrorState = ERROR_STATE()
		 , @$ErrorLine = ERROR_LINE()
		 , @$ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-')
		 , @$ErrorMessage = ISNULL(ERROR_MESSAGE(),'NO MESSAGE')

	/* Case 2 Expected Error*****/
	IF @ReturnValue != 0 BEGIN
		SELECT @$ErrorNumber = @ReturnValue
			 , @$ErrorSeverity = 16
			 , @$ErrorState = 1
			 , @$ErrorLine = 0
			 , @$ErrorProcedure = ISNULL(@$prog, '-')
			 , @$ErrorMessage = ISNULL((SELECT Memo FROM @ErrCodeTbl WHERE ErrorCode = @ReturnValue),'Not Defined')
	END
	-- Building the message string that will contain original
		-- error information.
	SELECT @$ErrorMessage = N'Error %d, Level %d, State %d, Procedure %s, Line %d, ' + 'Message: ' + @$ErrorMessage

	IF @$ErrorNumber = 0
		SELECT @$ErrorMessage = 'Unknown'
	IF @$prev_trancount = 0 and @@TRANCOUNT > 0
		Rollback transaction
		-- Raise an error: msg_str parameter of RAISERROR will contain
		-- the original error information.
	RAISERROR (@ErrorMessage, @ErrorSeverity, @ErrorState, @ErrorNumber, -- parameter: original error number.
	  @ErrorSeverity, -- parameter: original error severity.
	  @ErrorState, -- parameter: original error state.
	  @ErrorProcedure, -- parameter: original error procedure name.
	  @ErrorLine-- parameter: original error line number.
	)

	RETURN @ReturnValue

DONE:
IF @$prev_trancount = 0 and @@TRANCOUNT > 0 BEGIN
	COMMIT TRANSACTION
END

RETURN @ReturnValue

END