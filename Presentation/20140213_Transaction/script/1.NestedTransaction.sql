USE STUDY
GO

IF OBJECT_ID('NestedTran') IS NOT NULL
	DROP TABLE NestedTran
GO
CHECKPOINT

SELECT * FROM fn_dblog(null, null)

CREATE TABLE NestedTran
(
Col1 VARCHAR(255)
)


TRUNCATE TABLE NestedTran


BEGIN TRAN A
	INSERT NestedTran VALUES ('A')
	
	BEGIN TRAN B
		INSERT NestedTran VALUES ('B')
	ROLLBACK TRAN B

COMMIT TRAN A
GO

SELECT * FROM NestedTran
SELECT @@TRANCOUNT

--Question 1. can be possible? what's the @@TRANCOUNT RESULT
BEGIN TRAN A
	INSERT NestedTran VALUES ('A')

	BEGIN TRAN BB
		INSERT NestedTran VALUES ('BB')
		BEGIN TRAN CCC
			INSERT NestedTran VALUES ('CCC')
			SELECT @@TRANCOUNT
			SELECT * FROM NestedTran
		COMMIT TRAN CCC
		SELECT @@TRANCOUNT
		SELECT * FROM NestedTran

	COMMIT TRAN A
	SELECT @@TRANCOUNT
	SELECT * FROM NestedTran
COMMIT TRAN BB
SELECT @@TRANCOUNT
SELECT * FROM NestedTran


--Question 2. Is it possible? what's the @@TRANCOUNT RESULT
BEGIN TRAN A
	BEGIN TRAN BB
		BEGIN TRAN CCC
			SELECT @@TRANCOUNT

		COMMIT TRAN CCC
		SELECT @@TRANCOUNT

	COMMIT TRAN
	SELECT @@TRANCOUNT
COMMIT TRAN BB
SELECT @@TRANCOUNT


--Question 2. Is it possible? what's the @@TRANCOUNT RESULT
BEGIN TRAN A
	BEGIN TRAN BB
		BEGIN TRAN CCC
			SELECT @@TRANCOUNT

		COMMIT TRAN CCC
		SELECT @@TRANCOUNT

	COMMIT TRAN
	SELECT @@TRANCOUNT
COMMIT TRAN BB
SELECT @@TRANCOUNT

--Question 3. Is it possible? what's the @@TRANCOUNT RESULT
BEGIN TRAN A
	BEGIN TRAN BB
		BEGIN TRAN CCC
			SELECT @@TRANCOUNT

		COMMIT TRAN CCC
		SELECT @@TRANCOUNT

	ROLLBACK TRAN BB
	SELECT @@TRANCOUNT
COMMIT TRAN A
SELECT @@TRANCOUNT

--Question 4. Is it possible? what's the @@TRANCOUNT RESULT
BEGIN TRAN A
	BEGIN TRAN BB
		BEGIN TRAN CCC
			SELECT @@TRANCOUNT

		ROLLBACK TRAN
		SELECT @@TRANCOUNT

	ROLLBACK TRAN
	SELECT @@TRANCOUNT
COMMIT TRAN A
SELECT @@TRANCOUNT


-- Question 5. Truncate table can be rollback?

INSERT INTO NestedTran VALUES('A')
GO 1000

checkpoint

SELECT * FROM fn_dblog(null, null)
SELECT COUNT(*) FROM NestedTran 

checkpoint
BEGIN TRANSACTION
TRUNCATE TABLE NestedTran
ROLLBACK TRANSACTION
SELECT * FROM fn_dblog(null, null)

checkpoint
BEGIN TRANSACTION
delete from NestedTran
ROLLBACK TRANSACTION
SELECT * FROM fn_dblog(null, null)


SELECT COUNT(*) FROM NestedTran 



-- Question 6. which database recovery mode is the fastest one for this batch
SET STATISTICS IO ON
SET STATISTICS TIME ON
ALTER DATABASE Study SET RECOVERY SIMPLE
ALTER DATABASE Study SET RECOVERY BULK_LOGGED
ALTER DATABASE Study SET RECOVERY FULL

TRUNCATE TABLE NestedTran
checkpoint

INSERT INTO NestedTran
SELECT TOP 100000 'A'
  FROM sys.objects a
  CROSS JOIN sys.objects b
  CROSS JOIN sys.objects c
  CROSS JOIN sys.objects d
  CROSS JOIN sys.objects e
  CROSS JOIN sys.objects f


print '--------------------------------'
go

--SELECT * FROM sys.fn_dblog(null,null)

backup database study to disk='nul'

TRUNCATE TABLE NestedTran
checkpoint

INSERT INTO NestedTran WITH(TABLOCK)
SELECT TOP 100000 'A'
  FROM sys.objects a
  CROSS JOIN sys.objects b
  CROSS JOIN sys.objects c
  CROSS JOIN sys.objects d
  CROSS JOIN sys.objects e
  CROSS JOIN sys.objects f

SELECT * FROM sys.fn_dblog(null,null)

--truncate table NestedTran
--SELECT * FROM sys.fn_dblog(null,null)
--checkpoint

/*
A SQL Server DBA myth a day: (26/30) nested transactions are real
http://www.sqlskills.com/blogs/paul/a-sql-server-dba-myth-a-day-2630-nested-transactions-are-real/

*/
