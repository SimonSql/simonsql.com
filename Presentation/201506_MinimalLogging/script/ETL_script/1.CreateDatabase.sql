USE master
GO

-- Create datbase name "Simon_Demo"
IF DB_ID('Simon_Demo') IS NOT NULL
	DROP DATABASE Simon_Demo
go
CREATE DATABASE [Simon_Demo]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'Simon_Demo', FILENAME = N'E:\SQLData\SQL2014\Simon_Demo.mdf' , SIZE = 1024MB , MAXSIZE = UNLIMITED, FILEGROWTH = 256MB )
 LOG ON 
( NAME = N'Simon_Demo_log', FILENAME = N'E:\SQLData\SQL2014\Simon_Demo_log.ldf' , SIZE = 1024MB , MAXSIZE = UNLIMITED , FILEGROWTH = 256MB)
GO

ALTER DATABASE Simon_Demo SET RECOVERY FULL
GO

--To create backup chain.
BACKUP DATABASE Simon_Demo TO DISK='NUL'
GO

ALTER DATABASE Simon_Demo SET RECOVERY BULK_LOGGED
GO


USE Simon_Demo
GO

CREATE TABLE tbl_1gb
(
Idx INT IDENTITY(1,1)
, Col1 CHAR(4000)
, Col2 CHAR(4000)
)
GO
BACKUP log Simon_Demo TO DISK='NUL'
GO
CHECKPOINT

--ALTER TABLE tbl_1gb ADD  CONSTRAINT [PK_tbl_1gb] PRIMARY KEY CLUSTERED 
--(
--	Idx ASC
--)--WITH (DATA_COMPRESSION=PAGE)
--GO
DECLARE @col1 CHAR(4000), @col2 CHAR(4000)
SET @col1 = REPLICATE('A',4000)
SET @col2 = REPLICATE('B',4000)

INSERT INTO tbl_1gb (Col1,Col2)
SELECT TOP (1024/8*1024) @col1 AS Col1, @col2 AS Col2
FROM master.dbo.spt_values a
CROSS JOIN master.dbo.spt_values b
CROSS JOIN master.dbo.spt_values c
CROSS JOIN master.dbo.spt_values d
CROSS JOIN master.dbo.spt_values e

--SELECT TOP (1024/8*1024) IDENTITY(INT,1,1) AS Idx, @col1 AS Col1, @col2 AS Col2
--  INTO tbl_1gb
--FROM master.dbo.spt_values a
--CROSS JOIN master.dbo.spt_values b
--CROSS JOIN master.dbo.spt_values c
--CROSS JOIN master.dbo.spt_values d
--CROSS JOIN master.dbo.spt_values e

--SET IDENTITY_INSERT tbl_1gb off


EXEC sp_spaceused 'tbl_1gb'

ALTER DATABASE Simon_Demo SET RECOVERY FULL
GO


SELECT COUNT(*) FROM sys.fn_dblog(NULL,NULL)