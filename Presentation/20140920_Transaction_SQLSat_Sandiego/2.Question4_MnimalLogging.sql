use Demo
GO

/*******************************************************
Question 4. Which recovery mode is the fastest for this query?
*******************************************************/
TRUNCATE TABLE Tbl
GO
INSERT INTO Tbl
SELECT TOP 1000000 'A'
  FROM sys.objects a
  CROSS JOIN sys.objects b
  CROSS JOIN sys.objects c
  CROSS JOIN sys.objects d
  CROSS JOIN sys.objects e
  CROSS JOIN sys.objects f
GO

