use Simon_Source
go
--TRUNCATE TABLE #tmp

CREATE TABLE #tmp
(AccountID BIGINT)
GO

INSERT INTO #tmp
        (AccountID)
SELECT 1
GO 1000

SET STATISTICS PROFILE on
SET STATISTICS IO ON
SELECT a.*
  FROM dbo.[Accounts] a
  JOIN #tmp b ON a.accountid = b.accountid
 
SELECT a.*
  FROM dbo.[Accounts] a
  JOIN dbo.Num b ON n BETWEEN 1 AND 1000
 WHERE AccountID=1


SELECT * 
  FROM [Account].[Accounts] 
 WHERE AccountID BETWEEN 100 AND 500
