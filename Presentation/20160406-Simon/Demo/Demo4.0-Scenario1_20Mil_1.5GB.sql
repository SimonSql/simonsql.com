use Simon_Source
go

CREATE TABLE num
(N INT IDENTITY(1,1) PRIMARY KEY)
GO

INSERT INTO num DEFAULT VALUES
GO 10000


create TABLE S1_SimonTest
(
Seq INT
,col1 CHAR(10)
,col2 CHAR(10)
,col3 CHAR(50)
)
GO

USE Simon_Source
GO

TRUNCATE TABLE S1_SimonTest
GO

INSERT INTO S1_SimonTest WITH(TABLOCK)
SELECT ROW_NUMBER() OVER (ORDER BY GETDATE())
	 , REPLICATE('a', 10)
	 , REPLICATE('b', 10)
	 , REPLICATE('c', 50)
  FROM num n1
JOIN num n2 ON n2.n BETWEEN 1 AND 2000
--1.5GB. 20 Mil
GO