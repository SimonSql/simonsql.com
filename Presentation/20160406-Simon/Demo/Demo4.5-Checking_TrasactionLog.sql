USE Simon_target_USB
GO

EXEC master.dbo.sp_now




SELECT DB_NAME() 
	 , ISNULL(fg.groupid,0)
	 , ISNULL(fg.groupname,'Log')
	 , f.fileid
	 , (CASE WHEN CONVERT(BIT, f.status & 0x100000) = 1 THEN f.growth ELSE f.growth/128 End) as growth
	 , CONVERT(BIT, f.status & 0x100000) AS IsPercent
	 , convert(DECIMAL(12,2),f.size/128.000) as file_size
	 , convert(DECIMAL(12,2),fileproperty(f.name,'SpaceUsed')/128.000) as space_used
	 , convert(DECIMAL(12,2),(f.size-fileproperty(f.name,'SpaceUsed'))/128.000) as free_space
	 , CONVERT(DECIMAL(5,2), 100-FILEPROPERTY(f.name,'SpaceUsed')/CONVERT(DECIMAL(12,2), f.size)*100) AS FreeSpaceRatio
	 , f.name
	 , f.filename
  FROM sys.sysfiles f WITH(NOLOCK)
  LEFT OUTER JOIN sys.sysfilegroups fg WITH(NOLOCK)
	ON f.groupid = fg.groupid


SELECT *,DB_NAME(database_id), database_transaction_log_record_count, database_transaction_log_bytes_used FROM sys.dm_tran_database_transactions
WHERE database_id = DB_ID('Simon_target_USB')