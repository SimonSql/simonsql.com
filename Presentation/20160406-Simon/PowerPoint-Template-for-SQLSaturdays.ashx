

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8" /><title>
	PowerPoint Template for SQLSaturdays  -  SQLSaturday Wiki
</title><link rel="alternate" title=" SQLSaturday Wiki" href="http://wiki.sqlsaturday.com/RSS.aspx" type="application/rss+xml" />
<link rel="alternate" title=" SQLSaturday Wiki - Discussions" href="http://wiki.sqlsaturday.com/RSS.aspx?Discuss=1" type="application/rss+xml" />
<link rel="stylesheet" media="print" href="Themes/Default/Print_Styles.css" type="text/css" />
<link rel="stylesheet" media="screen" href="Themes/Default/Screen_Styles.css" type="text/css" />
<link rel="stylesheet" href="Themes/Editor.css" type="text/css" />
<link rel="search" href="Search.aspx?OpenSearch=1" type="application/opensearchdescription+xml" title=" SQLSaturday Wiki - Search" /><script src="Themes/Default/Scripts.js" type="text/javascript"></script>
<link rel="shortcut icon" href="Themes/Default/Icon.ico" type="image/x-icon" />
<script type="text/javascript" src="JS/jquery-1.4.1.min.js"></script>
<script type="text/javascript" src="JS/Scripts.js"></script>
<script type="text/javascript" src="JS/SearchHighlight.js"></script>
</head>
<body>
    <form name="aspnetForm" method="post" action="Default.aspx?Page=PowerPoint-Template-for-SQLSaturdays&amp;NS=" id="aspnetForm">
<div>
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="/wEPDwUJMTQ3NjA4MDI2D2QWAmYPZBYCAgMPZBYCAgkPZBYEAhsPDxYCHg1BbnRoZW1WaXNpYmxlaGRkAicPZBYCZg8WAh4LXyFJdGVtQ291bnQCARYCAgEPZBYCZg8VBOMBR2V0RmlsZS5hc3B4P0ZpbGU9U1FMU2F0dXJkYXklMjBQb3dlcnBvaW50JTIwLSUyME5ldy5wcHR4JmFtcDtBc1N0cmVhbUF0dGFjaG1lbnQ9MSZhbXA7UHJvdmlkZXI9U2NyZXdUdXJuLldpa2kuUGx1Z2lucy5TcWxTZXJ2ZXIuU3FsU2VydmVyRmlsZXNTdG9yYWdlUHJvdmlkZXImYW1wO0lzUGFnZUF0dGFjaG1lbnQ9MSZhbXA7UGFnZT1Qb3dlclBvaW50LVRlbXBsYXRlLWZvci1TUUxTYXR1cmRheXMIRG93bmxvYWQhU1FMU2F0dXJkYXkgUG93ZXJwb2ludCAtIE5ldy5wcHR4CTM3OS42MSBLQmRkswx9+8cEGos6v/AV8ze76uaLrGA=" />
</div>

<script type="text/javascript">
//<![CDATA[
var Anthem_FormID = "aspnetForm";
//]]>
</script>
<script src="/WebResource.axd?d=S9QUkf2cCbHww203GMVLQ8MmsOQVIYpWwzOGI5B1jQd0CDvQMd_KrjDr4klD9AtuJ7bvoDf3P0Q_f3B1Ji91qfDVXKEtn2M-aWlpVPsb069Tukid0&amp;t=633995077780000000" type="text/javascript"></script>
<div>

	<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="CA0B0334" />
</div>
		<script type="text/javascript">
<!--
__BaseName = "ctl00_CphMaster_";
__ConfirmMessage = "Are you sure you want to proceed?";
// -->
</script>
		<script type="text/javascript">
		<!--
			function __GetServerElementById(id) {
				return document.getElementById(__BaseName + id);
			}
			function __RequestConfirm() {
				return confirm(__ConfirmMessage);
			}
		// -->
		</script>
    
        <div id="HeaderDiv">
	    <img src="/images/wiki.png" alt="SQLSaturday Logo" />

            <div style="float: right;">Welcome <a href="Language.aspx" class="systemlink" title="Select your language">Guest</a>, you are in: <select class="namespacedropdown" onchange="javascript:var sel = this.value; document.location = (sel != '' ? (sel + '.') : '') + 'Default.aspx';"><option selected="selected" value="">&lt;root&gt;</option></select> &bull; <a href="Login.aspx?Redirect=http%3a%2f%2fwiki.sqlsaturday.com%2fDefault.aspx%3fPage%3dPowerPoint-Template-for-SQLSaturdays%26NS%3d" class="systemlink" title="Login">Login</a></div><h1> SQLSaturday Wiki</h1>

        </div>
                   
        <div id="ContainerDiv">
                 
            <div id="SidebarDiv">
                <div id="SidebarHeaderDiv">
                    <!-- Used for layout purposes only -->
                </div>
                <div id="SidebarContentDiv">
                    <div style="float: right;">
<a href="RSS.aspx" title="Update notifications for  SQLSaturday Wiki (RSS 2.0)"><img src="Themes/Default/Images/RSS.png" alt="RSS" /></a>
<a href="RSS.aspx?Discuss=1" title="Update notifications for  SQLSaturday Wiki Discussions (RSS 2.0)"><img src="Themes/Default/Images/RSS-Discussion.png" alt="RSS" /></a></div>
<a href="Edit.aspx?Page=Shirts&amp;Section=0" class="editsectionlink"></a><h3 class="separator">Navigation<a class="headeranchor" id="Navigation_0" href="#Navigation_0" title="Link to this Section">&#0182;</a></h3><ul><li><b><a class="pagelink" href="MainPage.ashx" title="Main Page">Main Page</a></b><br /></li></ul><br /><ul><li><a class="systemlink" href="RandPage.aspx" title="Random Page">Random Page</a></li><li><a class="systemlink" href="Edit.aspx" title="Create a new Page">Create a new Page</a></li><li><a class="systemlink" href="AllPages.aspx" title="All Pages">All Pages</a></li><li><a class="systemlink" href="Category.aspx" title="Categories">Categories</a></li><li><a class="systemlink" href="NavPath.aspx" title="Navigation Paths">Navigation Paths</a><br /></li></ul><br /><ul><li><a class="systemlink" href="AdminHome.aspx" title="Administration">Administration</a></li><li><a class="systemlink" href="Upload.aspx" title="File Management">File Management</a><br /></li></ul><br /><ul><li><a class="systemlink" href="Register.aspx" title="Create Account">Create Account</a><br /></li></ul><br /><small><b>Search the wiki</b></small><br />
<script type="text/javascript"><!--
function _DoSearch_SB36799296bd3e4a30a392c96e72474850() { document.location = 'Search.aspx?AllNamespaces=1&FilesAndAttachments=1&Query=' + encodeURI(document.getElementById('SB36799296bd3e4a30a392c96e72474850').value); }
// -->
</script><input class="txtsearchbox" type="text" id="SB36799296bd3e4a30a392c96e72474850" onkeydown="javascript:var keycode; if(window.event) keycode = event.keyCode; else keycode = event.which; if(keycode == 10 || keycode == 13) { _DoSearch_SB36799296bd3e4a30a392c96e72474850(); return false; }" /> <big><a href="#" onclick="javascript:_DoSearch_SB36799296bd3e4a30a392c96e72474850(); return false;">&raquo;</a></big><br /><br /><a href="http://www.screwturn.eu" title="PoweredBy" target="_blank"><img src="Images/PoweredBy.png" alt="PoweredBy" /></a>

                </div>
                <div id="SidebarFooterDiv">
                    <!-- Used for layout purposes only -->
                </div>
            </div>
            <div id="MainDiv">
                <div id="MainHeaderDiv">
                    <!-- Used for layout purposes only -->
                </div>
                <div id="PageInternalHeaderDiv">
</div>
                

    <script type="text/javascript">
    <!--
        function __ShowAllTrail() {
            try {
                document.getElementById("BreadcrumbsDivMin").style["display"] = "none";
                document.getElementById("BreadcrumbsDivAll").style["display"] = "";
                __SetStatus("1");
            }
            catch(ex) { }
            return false;
        }
        function __HideTrail() {
            try {
                document.getElementById("BreadcrumbsDivMin").style["display"] = "";
                document.getElementById("BreadcrumbsDivAll").style["display"] = "none";
                __SetStatus("0");
            }
            catch(ex) { }
            return false;
        }
        
        function __SetStatus(open) {
            __CreateCookie("ScrewTurnWikiBCT", open, 365);
        }
        function __GetStatus() {
            var value = __ReadCookie("ScrewTurnWikiBCT");
            if(value) return value;
            else return "0";
        }

        function __InitBCT() {
        	if(__GetStatus() == "1") {
        		__ShowAllTrail();
        	}
        }

        var __attachmentsMenuJustShown = false;
        var __adminToolsMenuJustShown = false;
        var __ie7Mode = false;

        function __ToggleAttachmentsMenu(cx, cy) {
        	var element = document.getElementById("PageAttachmentsDiv");
        	if(element) {
        		if(element.style["display"] == "none") {
        			element.style["display"] = "";
        			var pos = __AbsolutePosition(element);
        			if(pos.left - cx > 0) {
        				__ie7Mode = true;
        				element.style["position"] = "absolute";
        				element.style["top"] = cy + "px";
        				element.style["left"] = (cx - pos.width) + "px";
        			}
        			else {
        				__RepositionDiv(document.getElementById("PageAttachmentsLink"), element);
        			}
        			__attachmentsMenuJustShown = true;
        		}
        	}
        	return false;
        }
        function __HideAttachmentsMenu() {
        	var element = document.getElementById("PageAttachmentsDiv");
        	if(element && !__attachmentsMenuJustShown) {
        		element.style["display"] = "none";
        		if (__ie7Mode) element.style["left"] = "10000px";
        	}
        	__attachmentsMenuJustShown = false;
        	return true; // Needed to enabled next clicks' action (file download)
        }

        function __ToggleAdminToolsMenu(cx, cy) {
        	var element = document.getElementById("AdminToolsDiv");
        	if(element) {
        		if(element.style["display"] == "none") {
        			element.style["display"] = "";
        			var pos = __AbsolutePosition(element);
        			if(pos.left - cx > 0) {
        				__ie7Mode = true;
        				element.style["position"] = "absolute";
        				element.style["top"] = cy + "px";
        				element.style["left"] = (cx - pos.width) + "px";
        			}
        			else {
        				__RepositionDiv(document.getElementById("AdminToolsLink"), element);
        			}
        			__adminToolsMenuJustShown = true;
        		}
        	}
        	return false;
        }
        function __HideAdminToolsMenu() {
        	var element = document.getElementById("AdminToolsDiv");
        	if(element && !__adminToolsMenuJustShown) {
        		element.style["display"] = "none";
        		if(__ie7Mode) element.style["left"] = "10000px";
        	}
        	__adminToolsMenuJustShown = false;
        	return true; // Needed to enable next clicks' action (admin tools)
        }

        function __HideAllMenus() {
        	__HideAttachmentsMenu();
        	__HideAdminToolsMenu();
        }

        document.body.onclick = __HideAllMenus;

        function __AbsolutePosition(obj) {
        	var pos = null;
        	if(obj != null) {
        		pos = new Object();
        		pos.top = obj.offsetTop;
        		pos.left = obj.offsetLeft;
        		pos.width = obj.offsetWidth;
        		pos.height = obj.offsetHeight;

        		obj = obj.offsetParent;
        		while(obj != null) {
        			pos.top += obj.offsetTop;
        			pos.left += obj.offsetLeft;
        			obj = obj.offsetParent;
        		}
        	}
        	return pos;
        }

        var __showTimer = null;
        var __hideTimer = null;

        function __ShowDropDown(e, divId, parent) {
           	// Set a timer
        	// On mouse out, cancel the timer and start a 2nd timer that hides the menu
        	// When the 1st timer elapses
        	//   show the drop-down
        	//   on menu mouseover, cancel the 2nd timer
        	//   on menu mouse out, hide the menu
        	__showTimer = setTimeout('__ShowDropDownForReal(' + e.clientX + ', ' + e.clientY + ', "' + divId + '", "' + parent.id + '");', 200);
        }

        function __ShowDropDownForReal(cx, cy, divId, parentId) {
        	var pos = __AbsolutePosition(document.getElementById(parentId));
        	var menu = document.getElementById(divId);

			// This is needed to trick IE7 which, for some reason,
			// does not position the drop-down correctly with the new Default theme
        	if(pos.left - cx > 30) {
        		menu.style["display"] = "";
        		menu.style["position"] = "absolute";
        		menu.style["top"] = cy + "px";
        		menu.style["left"] = (cx - 10) + "px";
        	}
        	else {
        		menu.style["display"] = "";
        		menu.style["position"] = "absolute";
        		menu.style["top"] = (pos.top + pos.height) + "px";
        		menu.style["left"] = pos.left + "px";
        	}
        	__showTimer = null;
        }

        function __HideDropDown(divId) {
        	if(__showTimer) clearTimeout(__showTimer);
        	__hideTimer = setTimeout('__HideDropDownForReal("' + divId + '");', 200);
        }

        function __HideDropDownForReal(divId) {
        	document.getElementById(divId).style["display"] = "none";
        	__hideTimer = null;
        }

        function __CancelHideTimer() {
        	if(__hideTimer) clearTimeout(__hideTimer);
        }
        
    // -->
    </script>

	<a id="PageTop"></a>
	
	<div id="PageHeaderDiv">
	
		<!-- Change this to PageToolbarDiv -->
		<div id="EditHistoryLinkDiv">
			<a id="DiscussLink" title="Discuss" href="PowerPoint-Template-for-SQLSaturdays.ashx?Discuss=1">Discuss (0)</a>
			
			<a id="ViewCodeLink" title="View Page Code" href="PowerPoint-Template-for-SQLSaturdays.ashx?Code=1">View Page Code</a>
			<a id="HistoryLink" title="View Page edit history" href="History.aspx?Page=PowerPoint-Template-for-SQLSaturdays">History</a>
			<a id="PageAttachmentsLink" title="Attachments" href="#" onclick="javascript:return __ToggleAttachmentsMenu(event.clientX, event.clientY);">Attachments</a>
			
			
			
			
		</div>
		
		<h1 class="pagetitle">
			
			PowerPoint Template for SQLSaturdays 
			
		</h1>
		
		<div id="PrintLinkDiv">
			<a id="PrintLink" href="Print.aspx?Page=PowerPoint-Template-for-SQLSaturdays" title="Printer friendly version" target="_blank">Print</a>
		</div>
		
		<div id="RssLinkDiv">
			<a id="RssLink" href="RSS.aspx?Page=PowerPoint-Template-for-SQLSaturdays" title="Update notifications for this Page (RSS 2.0)" target="_blank">RSS</a>
		</div>
		
		<div id="EmailNotificationDiv">
			<span id="Anthem_ctl00_CphMaster_btnEmailNotification__"></span>
		</div>
		
		<div id="ctl00_CphMaster_pnlPageInfo">
	
			<div id="PageInfoDiv">
				<span id="ModificationSpan">
					Modified on 
					2012/04/25 22:25
				</span>
				<span id="AuthorSpan">
					 by 
					<a href="User.aspx?Username=karlakay22%40msn.com">Karla Landrum</a>
				</span>
				<span id="NavPathsSpan">
					
				</span>
				<span id="CategoriesSpan">
					Categorized as 
					<a href="AllPages.aspx?Cat=Event%20Administration%20Tools" title="Event Administration Tools">Event Administration Tools</a>, <a href="AllPages.aspx?Cat=Event%20Management" title="Event Management">Event Management</a>
				</span>
				
				<span id="PageDiscussionSpan">
					
					
				</span>
			</div>
		
</div>
		
		<div id="BreadcrumbsDiv"><div id="BreadcrumbsDivMin">&raquo; <b><a href="PowerPoint-Template-for-SQLSaturdays.ashx" title="PowerPoint Template for SQLSaturdays ">PowerPoint Template for SQLSaturdays </a></b> </div><div id="BreadcrumbsDivAll" style="display: none;"><a href="#" onclick="javascript:return __HideTrail();" title="Click here to hide the Breadcrumbs Trail">[X]</a> &raquo; <b><a href="PowerPoint-Template-for-SQLSaturdays.ashx" title="PowerPoint Template for SQLSaturdays ">PowerPoint Template for SQLSaturdays </a></b> </div></div>
		
		
	
	</div>
	
	<div id="PageContentDiv">
		
The attached SQLSaturday and PASS branded template can be used by speakers and organizers to ensure all PowerPoints are consistent. Click the 'Attachment' button on the far right and you can download the file from there.<br /><br />You are not required to use this, but often a speaker, typically a new one, will ask if you have one available. Most events offer the speakers a standard .ppt to use for their presentations, so quite often they will ask.<br /><br />If using for your event, be sure to change the Master Slide in the template to your specific event's logo. <br /><br />Tip: Offer on one of your highest level sponsorships, such as Platinum, for a vendor to be the sole logo on your&nbsp;event's .ppt. 

	</div>
	
	
	
	<div id="PageAttachmentsDiv" style="position: absolute; left: 10000px;">
		<div id="Anthem_ctl00_CphMaster_attachmentViewer_rptItems__">
		<table id="AttachmentViewerTable" class="generic" cellpadding="0" cellspacing="0">
			<thead>
			<tr class="tableheader">
				<th>&nbsp;</th>
				<th>Name</th>
				<th>Size</th>
			</tr>
			</thead>
			<tbody>
	
		<tr class="tablerow">
			<td><img src="Images/File.png" alt="-" /></td>
			<td><a href='GetFile.aspx?File=SQLSaturday%20Powerpoint%20-%20New.pptx&amp;AsStreamAttachment=1&amp;Provider=ScrewTurn.Wiki.Plugins.SqlServer.SqlServerFilesStorageProvider&amp;IsPageAttachment=1&amp;Page=PowerPoint-Template-for-SQLSaturdays' title="Download">SQLSaturday Powerpoint - New.pptx</a></td>
			<td>379.61 KB</td>
		</tr>
	
		</tbody>
		</table>
	</div>

    </div>
    
    <div id="AdminToolsDiv" style="position: absolute; left: 10000px;">
		
		
		
    </div>
    
    <script type="text/javascript">
    <!--
    	function __RepositionDiv(link, element) {
    		var absPos = __AbsolutePosition(link);
    		var elemAbsPos = __AbsolutePosition(element);

    		element.style["top"] = (absPos.top + absPos.height) + "px";
    		element.style["left"] = (absPos.left - (elemAbsPos.width - absPos.width)) + "px";
    		element.style["position"] = "absolute";
    	}

		// Hide attachments and admin tools divs
    	// This is needed because __RepositionDiv cannot calculate the width of the element when it's hidden
    	var __elem = document.getElementById("PageAttachmentsDiv");
    	if(document.getElementById("PageAttachmentsLink")) {
    		__RepositionDiv(document.getElementById("PageAttachmentsLink"), __elem);
    	}
    	__elem.style["display"] = "none";

    	__elem = document.getElementById("AdminToolsDiv");
    	if(document.getElementById("AdminToolsLink")) {
    		__RepositionDiv(document.getElementById("AdminToolsLink"), __elem);
    	}
    	__elem.style["display"] = "none";

    	__InitBCT();
    // -->
    </script>


                <div id="PageInternalFooterDiv">
</div>
                <div id="MainFooterDiv">
                    <!-- Used for layout purposes only -->
                </div>
            </div>

        </div>
            
        <div id="FooterDiv">
            <p class="small"><a class="externallink" href="http://www.screwturn.eu" title="ScrewTurn Wiki" target="_blank">ScrewTurn Wiki</a> version 3.0.2.509. Some of the icons created by <a class="externallink" href="http://www.famfamfam.com" title="FamFamFam" target="_blank">FamFamFam</a>.</p>

        </div>

    </form>  
</body>
</html>
