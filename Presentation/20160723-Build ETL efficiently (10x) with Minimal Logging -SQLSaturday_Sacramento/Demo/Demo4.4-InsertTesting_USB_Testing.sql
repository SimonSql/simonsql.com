
USE [Simon_Target_USB]
GO
ALTER DATABASE [Simon_Target_USB] SET RECOVERY Simple
go


SELECT DB_NAME() 
	 , ISNULL(fg.groupid,0)
	 , ISNULL(fg.groupname,'Log')
	 , f.fileid
	 , (CASE WHEN CONVERT(BIT, f.status & 0x100000) = 1 THEN f.growth ELSE f.growth/128 End) as growth
	 , CONVERT(BIT, f.status & 0x100000) AS IsPercent
	 , convert(DECIMAL(12,2),f.size/128.000) as file_size
	 , convert(DECIMAL(12,2),fileproperty(f.name,'SpaceUsed')/128.000) as space_used
	 , convert(DECIMAL(12,2),(f.size-fileproperty(f.name,'SpaceUsed'))/128.000) as free_space
	 , CONVERT(DECIMAL(5,2), 100-FILEPROPERTY(f.name,'SpaceUsed')/CONVERT(DECIMAL(12,2), f.size)*100) AS FreeSpaceRatio
	 , f.name
	 , f.filename
  FROM sys.sysfiles f WITH(NOLOCK)
  LEFT OUTER JOIN sys.sysfilegroups fg WITH(NOLOCK)
	ON f.groupid = fg.groupid

--Base Table
/*
Simon_Target_USB
USE [Simon_Target]
GO

--DROP TABLE [dbo].[S1_SimonTest_Target]
CREATE TABLE [dbo].[S1_SimonTest_Target](
	[Seq] [INT] NULL,
	[col1] [VARCHAR](10) NULL,
	[col2] [VARCHAR](10) NULL,
	[col3] [VARCHAR](50) NULL
) ON [PRIMARY]
GO

CREATE CLUSTERED INDEX CL01 ON [S1_SimonTest_Target] ([Seq])
CREATE NONCLUSTERED INDEX nc01 ON [S1_SimonTest_Target] (col1)
CREATE NONCLUSTERED INDEX nc02 ON [S1_SimonTest_Target] (col2)
CREATE NONCLUSTERED INDEX nc03 ON [S1_SimonTest_Target] (col3)
GO
*/

EXEC sp_spaceused 'S1_SimonTest_Target'
GO

/*********************************
<Method 3 – Simple is the best!>
Truncate table Target_B
Insert Target_B select * from Source_A
*********************************/
CHECKPOINT
WAITFOR DELAY '00:00:10'
GO
CHECKPOINT
SELECT COUNT(*) AS LogRecordCount, SUM([Log Record Length]) FROM sys.fn_dblog(NULL,NULL)
GO

DECLARE @start_datetime DATETIME = GETDATE(), @end_dateatime DATETIME

BEGIN TRAN

TRUNCATE TABLE S1_SimonTest_Target
INSERT INTO S1_SimonTest_Target
SELECT * FROM [SELF].Simon_Source.dbo.S1_SimonTest

SET @end_dateatime = GETDATE()
SELECT 'Method3', DATEDIFF(ss, @start_datetime, @end_dateatime) AS Duration_Second, COUNT(*) AS LogRecordCount, SUM(CONVERT(BIGINT,[Log Record Length])) Sum_LogRecordLength FROM sys.fn_dblog(NULL,NULL)



SELECT DB_NAME() 
	 , ISNULL(fg.groupid,0)
	 , ISNULL(fg.groupname,'Log')
	 , f.fileid
	 , (CASE WHEN CONVERT(BIT, f.status & 0x100000) = 1 THEN f.growth ELSE f.growth/128 End) as growth
	 , CONVERT(BIT, f.status & 0x100000) AS IsPercent
	 , convert(DECIMAL(12,2),f.size/128.000) as file_size
	 , convert(DECIMAL(12,2),fileproperty(f.name,'SpaceUsed')/128.000) as space_used
	 , convert(DECIMAL(12,2),(f.size-fileproperty(f.name,'SpaceUsed'))/128.000) as free_space
	 , CONVERT(DECIMAL(5,2), 100-FILEPROPERTY(f.name,'SpaceUsed')/CONVERT(DECIMAL(12,2), f.size)*100) AS FreeSpaceRatio
	 , f.name
	 , f.filename
  FROM sys.sysfiles f WITH(NOLOCK)
  LEFT OUTER JOIN sys.sysfilegroups fg WITH(NOLOCK)
	ON f.groupid = fg.groupid


ROLLBACK TRAN
GO
/*********************************
<Method 4>
Truncate table
Drop all Nonclustered Indexes
Drop Clustered Index
Insert Target_B with(TabLock)Select * from Source_A
Create Clustered Index
Create all Nonclustered Indexes
*********************************/
CHECKPOINT
WAITFOR DELAY '00:00:10'
GO

CHECKPOINT
SELECT COUNT(*) AS LogRecordCount, SUM([Log Record Length]) FROM sys.fn_dblog(NULL,NULL)
GO

DECLARE @start_datetime DATETIME = GETDATE(), @end_dateatime DATETIME

BEGIN TRAN

TRUNCATE TABLE S1_SimonTest_Target
DROP INDEX nc01 ON [S1_SimonTest_Target]
DROP INDEX nc02 ON [S1_SimonTest_Target]
DROP INDEX nc03 ON [S1_SimonTest_Target]
DROP INDEX CL01 ON [S1_SimonTest_Target]

INSERT INTO S1_SimonTest_Target WITH(TABLOCK)
SELECT * FROM [SELF].Simon_Source.dbo.S1_SimonTest

CREATE CLUSTERED INDEX CL01 ON [S1_SimonTest_Target] ([Seq])
CREATE NONCLUSTERED INDEX nc01 ON [S1_SimonTest_Target] (col1)
CREATE NONCLUSTERED INDEX nc02 ON [S1_SimonTest_Target] (col2)
CREATE NONCLUSTERED INDEX nc03 ON [S1_SimonTest_Target] (col3)

SET @end_dateatime = GETDATE()
SELECT 'Method4', DATEDIFF(ss, @start_datetime, @end_dateatime) AS Duration_Second, COUNT(*) AS LogRecordCount, SUM(CONVERT(BIGINT,[Log Record Length])) Sum_LogRecordLength FROM sys.fn_dblog(NULL,NULL)



SELECT DB_NAME() 
	 , ISNULL(fg.groupid,0)
	 , ISNULL(fg.groupname,'Log')
	 , f.fileid
	 , (CASE WHEN CONVERT(BIT, f.status & 0x100000) = 1 THEN f.growth ELSE f.growth/128 End) as growth
	 , CONVERT(BIT, f.status & 0x100000) AS IsPercent
	 , convert(DECIMAL(12,2),f.size/128.000) as file_size
	 , convert(DECIMAL(12,2),fileproperty(f.name,'SpaceUsed')/128.000) as space_used
	 , convert(DECIMAL(12,2),(f.size-fileproperty(f.name,'SpaceUsed'))/128.000) as free_space
	 , CONVERT(DECIMAL(5,2), 100-FILEPROPERTY(f.name,'SpaceUsed')/CONVERT(DECIMAL(12,2), f.size)*100) AS FreeSpaceRatio
	 , f.name
	 , f.filename
  FROM sys.sysfiles f WITH(NOLOCK)
  LEFT OUTER JOIN sys.sysfilegroups fg WITH(NOLOCK)
	ON f.groupid = fg.groupid


ROLLBACK TRAN
GO
