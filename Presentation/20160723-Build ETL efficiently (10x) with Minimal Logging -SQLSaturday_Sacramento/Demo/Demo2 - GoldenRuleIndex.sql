USE Temp
GO

CREATE TABLE GoldenRuleIndex
(
AccountID INT NOT null
, Create_Datetime DATETIME NOT null
, IsDeleted BIT NOT null
, FirstName VARCHAR(255) NOT null
, LastName VARCHAR(255) null
, Col1 VARCHAR(255) null
, Col2 VARCHAR(255) null
, Col3 VARCHAR(255) null
, Col4 VARCHAR(255) null
, Col5 VARCHAR(255) null
)
GO

SELECT * 
  FROM GoldenRuleIndex
 WHERE Firstname LIKE 'abc%'
   AND Create_Datetime BETWEEN '2015-01-01' AND '2016-01-01'
   AND IsDeleted = 0


CREATE INDEX nc01 ON GoldenRuleIndex(Firstname,Create_Datetime,IsDeleted)
CREATE INDEX nc02 ON GoldenRuleIndex(Firstname,IsDeleted,Create_Datetime)

CREATE INDEX nc03 ON GoldenRuleIndex(Create_Datetime,Firstname,IsDeleted)
CREATE INDEX nc04 ON GoldenRuleIndex(Create_Datetime,IsDeleted,Firstname)

CREATE INDEX nc05 ON GoldenRuleIndex(IsDeleted,Firstname,Create_Datetime)
CREATE INDEX nc06 ON GoldenRuleIndex(IsDeleted,Create_Datetime,Firstname)


SELECT * 
  FROM GoldenRuleIndex WITH(INDEX(nc05_IsDeleted))
 WHERE Firstname LIKE 'Sabc%'
   AND Create_Datetime BETWEEN '2015-01-01' AND '2016-01-01'
   AND IsDeleted = 0

SELECT * 
  FROM GoldenRuleIndex WITH(INDEX(nc06_IsDeleted))
 WHERE Firstname LIKE 'Sabc%'
   AND Create_Datetime BETWEEN '2015-01-01' AND '2016-01-01'
   AND IsDeleted = 0