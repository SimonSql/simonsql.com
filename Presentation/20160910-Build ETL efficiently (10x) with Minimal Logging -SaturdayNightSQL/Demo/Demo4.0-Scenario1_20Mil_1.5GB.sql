use Simon_Source
go

CREATE TABLE num
(N INT IDENTITY(1,1) PRIMARY KEY)
GO

INSERT INTO num DEFAULT VALUES
GO 10000


create TABLE S1_SimonTest
(
Seq INT
,col1 CHAR(10)
,col2 CHAR(10)
,col3 CHAR(50)
)
GO

USE Simon_Source
GO

/************************************************
Full Logging
************************************************/
TRUNCATE TABLE S1_SimonTest
GO

CHECKPOINT
go
SELECT TOP 100000 [Current LSN], Operation, Context, [Log Record Fixed Length], [Log Record Length], AllocUnitName, [Page ID], [Checkpoint Begin], [Checkpoint Begin], Description, [Xact ID]  
FROM fn_dblog(null, null)
GO

INSERT INTO S1_SimonTest --WITH(TABLOCK)
SELECT ROW_NUMBER() OVER (ORDER BY GETDATE())
	 , REPLICATE('a', 10)
	 , REPLICATE('b', 10)
	 , REPLICATE('c', 50)
  FROM num n1
JOIN num n2 ON n2.n BETWEEN 1 AND 200
-- 2 Mil
GO

SELECT Operation, COUNT(*) AS LogRecordCount, SUM([Log Record Length]) AS [Sum of Log Record Length] FROM sys.fn_dblog(NULL,NULL)
WHERE AllocUnitName='dbo.S1_SimonTest'
GROUP BY Operation
GO


/************************************************
Minimal Logging
************************************************/
TRUNCATE TABLE S1_SimonTest
GO

CHECKPOINT
go

SELECT [Current LSN], Operation, Context, [Log Record Fixed Length], [Log Record Length], AllocUnitName, [Page ID], [Checkpoint Begin], [Checkpoint Begin], Description, [Xact ID]  
FROM fn_dblog(null, null)
GO

INSERT INTO S1_SimonTest WITH(TABLOCK)
SELECT ROW_NUMBER() OVER (ORDER BY GETDATE())
	 , REPLICATE('a', 10)
	 , REPLICATE('b', 10)
	 , REPLICATE('c', 50)
  FROM num n1
JOIN num n2 ON n2.n BETWEEN 1 AND 200
-- 2 Mil
GO

SELECT Operation, COUNT(*) AS LogRecordCount, SUM([Log Record Length]) AS [Sum of Log Record Length] FROM sys.fn_dblog(NULL,NULL)
WHERE AllocUnitName='dbo.S1_SimonTest'
GROUP BY Operation
GO
