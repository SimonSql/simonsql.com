
USE [Simon_Target]
GO
ALTER DATABASE [Simon_Target] SET RECOVERY FULL
BACKUP DATABASE [Simon_Target] TO DISK='nul' WITH STATS=1
BACKUP log [Simon_Target] TO DISK='nul' WITH STATS=1
go

--Base Table
/*

USE [Simon_Target]
GO

--DROP TABLE [dbo].[S1_SimonTest_Target]
CREATE TABLE [dbo].[S1_SimonTest_Target](
	[Seq] [INT] NULL,
	[col1] [VARCHAR](10) NULL,
	[col2] [VARCHAR](10) NULL,
	[col3] [VARCHAR](50) NULL
) ON [PRIMARY]
GO

CREATE CLUSTERED INDEX CL01 ON [S1_SimonTest_Target] ([Seq])
CREATE NONCLUSTERED INDEX nc01 ON [S1_SimonTest_Target] (col1)
CREATE NONCLUSTERED INDEX nc02 ON [S1_SimonTest_Target] (col2)
CREATE NONCLUSTERED INDEX nc03 ON [S1_SimonTest_Target] (col3)
GO
*/

EXEC sp_spaceused 'S1_SimonTest_Target'
GO

/*********************************
<Method1>
Drop clustered index
Drop all nonclustered indexes
Truncate table Target_B
Insert Target_Bselect * from Source_A
Create clustered index
Create all nonclustered indexes
*********************************/
CHECKPOINT
WAITFOR DELAY '00:00:10'
GO
BACKUP log [Simon_Target] TO DISK='nul' WITH STATS=1
go
CHECKPOINT
SELECT COUNT(*) AS LogRecordCount, SUM([Log Record Length]) FROM sys.fn_dblog(NULL,NULL)
GO

DECLARE @start_datetime DATETIME = GETDATE(), @end_dateatime DATETIME

BEGIN TRAN

DROP INDEX CL01 ON [S1_SimonTest_Target]
DROP INDEX nc01 ON [S1_SimonTest_Target]
DROP INDEX nc02 ON [S1_SimonTest_Target]
DROP INDEX nc03 ON [S1_SimonTest_Target]
TRUNCATE TABLE S1_SimonTest_Target
INSERT INTO S1_SimonTest_Target
SELECT * FROM [SELF].Simon_Source.dbo.S1_SimonTest
CREATE CLUSTERED INDEX CL01 ON [S1_SimonTest_Target] ([Seq])
CREATE NONCLUSTERED INDEX nc01 ON [S1_SimonTest_Target] (col1)
CREATE NONCLUSTERED INDEX nc02 ON [S1_SimonTest_Target] (col2)
CREATE NONCLUSTERED INDEX nc03 ON [S1_SimonTest_Target] (col3)

SET @end_dateatime = GETDATE()
SELECT 'Method1', DATEDIFF(ss, @start_datetime, @end_dateatime) AS Duration_Second, COUNT(*) AS LogRecordCount, SUM([Log Record Length]) Sum_LogRecordLength FROM sys.fn_dblog(NULL,NULL)

ROLLBACK TRAN
GO


/*********************************
<Method 2>
Drop Table Target_B
Create table Target_B
Create clustered index
Create all nonclustered index
Insert Target_B select * from Source_A
*********************************/
CHECKPOINT
WAITFOR DELAY '00:00:10'
GO
BACKUP log [Simon_Target] TO DISK='nul' WITH STATS=1
go
CHECKPOINT
SELECT COUNT(*) AS LogRecordCount, SUM([Log Record Length]) FROM sys.fn_dblog(NULL,NULL)
GO

DECLARE @start_datetime DATETIME = GETDATE(), @end_dateatime DATETIME

BEGIN TRAN

DROP TABLE [dbo].[S1_SimonTest_Target]
CREATE TABLE [dbo].[S1_SimonTest_Target](
	[Seq] [INT] NULL,
	[col1] [VARCHAR](10) NULL,
	[col2] [VARCHAR](10) NULL,
	[col3] [VARCHAR](50) NULL
) ON [PRIMARY]

CREATE CLUSTERED INDEX CL01 ON [S1_SimonTest_Target] ([Seq])
CREATE NONCLUSTERED INDEX nc01 ON [S1_SimonTest_Target] (col1)
CREATE NONCLUSTERED INDEX nc02 ON [S1_SimonTest_Target] (col2)
CREATE NONCLUSTERED INDEX nc03 ON [S1_SimonTest_Target] (col3)

INSERT INTO S1_SimonTest_Target
SELECT * FROM [SELF].Simon_Source.dbo.S1_SimonTest

SET @end_dateatime = GETDATE()
SELECT 'Method2', DATEDIFF(ss, @start_datetime, @end_dateatime) AS Duration_Second, COUNT(*) AS LogRecordCount, SUM(CONVERT(BIGINT,[Log Record Length])) Sum_LogRecordLength FROM sys.fn_dblog(NULL,NULL)

ROLLBACK TRAN
GO
/*********************************
<Method 3 – Simple is the best!>
Truncate table Target_B
Insert Target_B select * from Source_A
*********************************/
CHECKPOINT
WAITFOR DELAY '00:00:10'
GO
BACKUP log [Simon_Target] TO DISK='nul' WITH STATS=1
go
CHECKPOINT
SELECT COUNT(*) AS LogRecordCount, SUM([Log Record Length]) FROM sys.fn_dblog(NULL,NULL)
GO

DECLARE @start_datetime DATETIME = GETDATE(), @end_dateatime DATETIME

BEGIN TRAN

TRUNCATE TABLE S1_SimonTest_Target
INSERT INTO S1_SimonTest_Target
SELECT * FROM [SELF].Simon_Source.dbo.S1_SimonTest

SET @end_dateatime = GETDATE()
SELECT 'Method3', DATEDIFF(ss, @start_datetime, @end_dateatime) AS Duration_Second, COUNT(*) AS LogRecordCount, SUM(CONVERT(BIGINT,[Log Record Length])) Sum_LogRecordLength FROM sys.fn_dblog(NULL,NULL)

ROLLBACK TRAN
GO
/*********************************
<Method 4>
Truncate table
Drop all Nonclustered Indexes
Drop Clustered Index
Insert Target_B with(TabLock)Select * from Source_A
Create Clustered Index
Create all Nonclustered Indexes
*********************************/
CHECKPOINT
WAITFOR DELAY '00:00:10'
GO
BACKUP log [Simon_Target] TO DISK='nul' WITH STATS=1
go
CHECKPOINT
SELECT COUNT(*) AS LogRecordCount, SUM([Log Record Length]) FROM sys.fn_dblog(NULL,NULL)
GO

DECLARE @start_datetime DATETIME = GETDATE(), @end_dateatime DATETIME

BEGIN TRAN

TRUNCATE TABLE S1_SimonTest_Target
DROP INDEX nc01 ON [S1_SimonTest_Target]
DROP INDEX nc02 ON [S1_SimonTest_Target]
DROP INDEX nc03 ON [S1_SimonTest_Target]
DROP INDEX CL01 ON [S1_SimonTest_Target]

INSERT INTO S1_SimonTest_Target WITH(TABLOCK)
SELECT * FROM [SELF].Simon_Source.dbo.S1_SimonTest

CREATE CLUSTERED INDEX CL01 ON [S1_SimonTest_Target] ([Seq])
CREATE NONCLUSTERED INDEX nc01 ON [S1_SimonTest_Target] (col1)
CREATE NONCLUSTERED INDEX nc02 ON [S1_SimonTest_Target] (col2)
CREATE NONCLUSTERED INDEX nc03 ON [S1_SimonTest_Target] (col3)

SET @end_dateatime = GETDATE()
SELECT 'Method4', DATEDIFF(ss, @start_datetime, @end_dateatime) AS Duration_Second, COUNT(*) AS LogRecordCount, SUM(CONVERT(BIGINT,[Log Record Length])) Sum_LogRecordLength FROM sys.fn_dblog(NULL,NULL)

ROLLBACK TRAN
GO

/*********************************
<Method 5>
Truncate table
Drop all Nonclustered Indexes only. Remain clustered index

Insert Target_B with(TabLock)Select * from Source_A
Create Clustered Index
Create all Nonclustered Indexes
*********************************/
CHECKPOINT
WAITFOR DELAY '00:00:10'
GO
BACKUP log [Simon_Target] TO DISK='nul' WITH STATS=1
go
CHECKPOINT
SELECT COUNT(*) AS LogRecordCount, SUM([Log Record Length]) FROM sys.fn_dblog(NULL,NULL)
GO

DECLARE @start_datetime DATETIME = GETDATE(), @end_dateatime DATETIME

BEGIN TRAN

TRUNCATE TABLE S1_SimonTest_Target
DROP INDEX nc01 ON [S1_SimonTest_Target]
DROP INDEX nc02 ON [S1_SimonTest_Target]
DROP INDEX nc03 ON [S1_SimonTest_Target]
DROP INDEX CL01 ON [S1_SimonTest_Target]

INSERT INTO S1_SimonTest_Target WITH(TABLOCK)
SELECT * FROM [SELF].Simon_Source.dbo.S1_SimonTest

CREATE CLUSTERED INDEX CL01 ON [S1_SimonTest_Target] ([Seq])
CREATE NONCLUSTERED INDEX nc01 ON [S1_SimonTest_Target] (col1)
CREATE NONCLUSTERED INDEX nc02 ON [S1_SimonTest_Target] (col2)
CREATE NONCLUSTERED INDEX nc03 ON [S1_SimonTest_Target] (col3)

SET @end_dateatime = GETDATE()
SELECT 'Method5', DATEDIFF(ss, @start_datetime, @end_dateatime) AS Duration_Second, COUNT(*) AS LogRecordCount, SUM(CONVERT(BIGINT,[Log Record Length])) Sum_LogRecordLength FROM sys.fn_dblog(NULL,NULL)

ROLLBACK TRAN
GO

/*********************************
Method 6
Drop Table Target_B
Select * into Target_B from Source_A
Create Clustered Index
Create all Nonclustered Indexes
*********************************/
CHECKPOINT
WAITFOR DELAY '00:00:10'
GO
BACKUP log [Simon_Target] TO DISK='nul' WITH STATS=1
go
CHECKPOINT
SELECT COUNT(*) AS LogRecordCount, SUM([Log Record Length]) FROM sys.fn_dblog(NULL,NULL)
GO

DECLARE @start_datetime DATETIME = GETDATE(), @end_dateatime DATETIME

BEGIN TRAN

DROP TABLE S1_SimonTest_Target

CREATE TABLE [dbo].[S1_SimonTest_Target](
	[Seq] [INT] NULL,
	[col1] [VARCHAR](10) NULL,
	[col2] [VARCHAR](10) NULL,
	[col3] [VARCHAR](50) NULL
) ON [PRIMARY]

INSERT INTO S1_SimonTest_Target WITH(TABLOCK)
SELECT * FROM [SELF].Simon_Source.dbo.S1_SimonTest

CREATE CLUSTERED INDEX CL01 ON [S1_SimonTest_Target] ([Seq])
CREATE NONCLUSTERED INDEX nc01 ON [S1_SimonTest_Target] (col1)
CREATE NONCLUSTERED INDEX nc02 ON [S1_SimonTest_Target] (col2)
CREATE NONCLUSTERED INDEX nc03 ON [S1_SimonTest_Target] (col3)

SET @end_dateatime = GETDATE()
SELECT 'Method6', DATEDIFF(ss, @start_datetime, @end_dateatime) AS Duration_Second, COUNT(*) AS LogRecordCount, SUM(CONVERT(BIGINT,[Log Record Length])) Sum_LogRecordLength FROM sys.fn_dblog(NULL,NULL)

ROLLBACK TRAN
GO