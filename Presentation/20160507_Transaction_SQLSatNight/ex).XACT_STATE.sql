Use Demo
-- Verify that the table does not exist.
IF OBJECT_ID (N'my_books', N'U') IS NOT NULL
    DROP TABLE my_books;
GO

CREATE TABLE my_books
    (
    Isbn        int PRIMARY KEY,
    Title       NVARCHAR(100)
    );
GO

SELECT XACT_STATE() as Before_Tran
BEGIN TRY	
    BEGIN TRANSACTION;
		INSERT my_books VALUES(1,'1')
		SELECT XACT_STATE() as After_Tran
        ALTER TABLE my_books DROP COLUMN author;      
    COMMIT TRANSACTION;

END TRY

BEGIN CATCH
    -- Test XACT_STATE for 0, 1, or -1.
    -- If 1, the transaction is committable.
    -- If -1, the transaction is uncommittable and should be rolled back.
    -- XACT_STATE = 0 means there is no transaction and a commit or rollback operation would generate an error.

    --Print Error Information 
    SELECT
            ERROR_NUMBER() AS ErrorNumber
            ,ERROR_SEVERITY() AS ErrorSeverity
            ,ERROR_STATE() AS ErrorState
            ,ERROR_PROCEDURE() AS ErrorProcedure
            ,ERROR_LINE() AS ErrorLine
            ,ERROR_MESSAGE() AS ErrorMessage;
	SELECT XACT_STATE() as Uncommittable_Tran
    -- Test whether the transaction is uncommittable.
    IF (XACT_STATE()) = -1
    BEGIN
        PRINT 'The transaction is in an uncommittable state.' + ' Rolling back transaction.'
        ROLLBACK TRANSACTION;
    END;

    -- Test whether the transaction is active and valid.
    IF (XACT_STATE()) = 1
    BEGIN
        PRINT 'The transaction is committable.' + ' Committing transaction.'
        COMMIT TRANSACTION;   
    END;
END CATCH;
GO 