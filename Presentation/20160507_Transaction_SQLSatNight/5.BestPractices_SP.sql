Use Demo
go

-- Data varification in Application is option.
-- Data varification in Database is mentantory.

CREATE TABLE dbo.ErrorLogs(
    errorLogSN        bigint           IDENTITY(1,1),
    loginName         nvarchar(128)    NOT NULL,
    hostName          nvarchar(128)    NOT NULL,
    errorNumber       int              NOT NULL,
    errorSeverity     int              NULL,
    errorState        int              NULL,
    errorProcedure    nvarchar(128)    NULL,
    errorLine         int              NULL,
    errorMessage      nvarchar(max)    NOT NULL,
    occurredDate      datetime         NOT NULL,
    CONSTRAINT PK_ErrorLogs PRIMARY KEY CLUSTERED (errorLogSN)
)
GO

CREATE PROCEDURE dbo.USP_InsertErrorLog
WITH EXECUTE AS OWNER
AS
BEGIN
	SET NOCOUNT ON;
	SET XACT_ABORT ON;

	DECLARE @ReturnValue int;

	DECLARE @$ErrorMessage nvarchar(4000)
		  , @$ErrorNumber int
		  , @$ErrorSeverity int
		  , @$ErrorState int
		  , @$ErrorLine int
		  , @$ErrorProcedure nvarchar(128)
	
	IF XACT_STATE() = -1
	BEGIN
		SET @ReturnValue = 1;
		GOTO ErrorHandler;
	END
	IF ERROR_NUMBER() IS NULL
	BEGIN
		SET @ReturnValue = 0;
		GOTO ErrorHandler;
	END

	SET @$ErrorNumber = ERROR_NUMBER();
	SET @$ErrorSeverity = ERROR_SEVERITY();
	SET @$ErrorState = ERROR_STATE();
	SET @$ErrorLine = ERROR_LINE();
	SET @$ErrorProcedure = ERROR_PROCEDURE();
	SET @$ErrorMessage = ERROR_MESSAGE();

	/**_## Log the error that occurred.*/
	INSERT dbo.ErrorLogs (loginName, hostName, errorNumber, errorSeverity, errorState, errorProcedure, errorLine, errorMessage, occurredDate)
	VALUES (CAST(ORIGINAL_LOGIN() AS nvarchar(128)), CAST(HOST_NAME() AS nvarchar(128)), @$ErrorNumber, @$ErrorSeverity, @$ErrorState, @$ErrorProcedure, @$ErrorLine, @$ErrorMessage, GETDATE());

	/**_# Rethrow the error.*/
	SET @$ErrorMessage = N'Error %d, Level %d, State %d, Procedure %s, Line %d, Message: ' + @$ErrorMessage;
	RAISERROR (@$ErrorMessage, @$ErrorSeverity, 1, @$ErrorNumber, @$ErrorSeverity, @$ErrorState, @$ErrorProcedure, @$ErrorLine);

	RETURN @$ErrorNumber;

	ErrorHandler:
		IF XACT_STATE() <> 0
			ROLLBACK TRANSACTION;

	RETURN 0;
END
GO


Create Procedure usp_Naming_Convention
@param int
--, ...
AS
BEGIN
	SET XACT_ABORT ON;
	SET NOCOUNT ON;
	--SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

	DECLARE @ReturnValue INT = 0
	DECLARE @ErrCodeTbl Table(ErrorCode int, Memo varchar(255))
	DECLARE @$ErrorMessage NVARCHAR(4000)
		  , @$ErrorNumber INT
		  , @$ErrorSeverity INT
		  , @$ErrorState INT
		  , @$ErrorLine INT
		  , @$ErrorProcedure NVARCHAR(200)

	DECLARE @$prog VARCHAR(50)
		  , @$proc_section_nm VARCHAR(50)
		  , @$error_db_name VARCHAR(50)

	/*
	Define error number
	50001 : ParameterMissing
	50002 : Data integrity problem...
	...
	0 : Success
	Else : @@Error
	*/
	
	INSERT INTO @ErrCodeTbl (ErrorCode, Memo)
	SELECT 50001, 'ParameterMissing' UNION ALL
	SELECT 50002, 'Data integrity problem'
			
	--<Vailidation Parameter values>
	--Defalut setting or update null value
	If @param is null BEGIN
		SET @ReturnValue = 50001
		GOTO ERROR
	END
       
	-- additional for non trans proc if a wrapper for transactional
	DECLARE @$prev_trancount INT, @$tran_flag_err INT;
		SET @$prev_trancount = @@TRANCOUNT;

	SELECT @$ErrorNumber = NULL
		 , @$ErrorMessage = NULL
		 , @$prog = LEFT(OBJECT_NAME(@@PROCID),50)
		 , @$error_db_name = db_name();

	IF @$prev_trancount = 0
		BEGIN TRANSACTION

	BEGIN TRY
		-- Step01 : Execute another SP. Get return value
		EXEC @ReturnValue = USP_INNER_TRAN 
		SET @ReturnValue = COALESCE(NULLIF(@ReturnValue, 0), @@ERROR)
		IF @ReturnValue != 0 BEGIN
			GOTO ERROR
		END

		-- Step02
		--Insert/update/delete any user code
		

		-- Step03
		--Dynamic SQL
		EXEC @ReturnValue = sp_executesql @stmt, @params, @values1, @values2 output
		SET @ReturnValue = COALESCE(NULLIF(@ReturnValue, 0), @@ERROR)

		IF @ReturnValue != 0 BEGIN
			GOTO ERROR
		END	ELSE BEGIN
			GOTO DONE
		END

	END TRY
	BEGIN CATCH
		/*
		UnExpected Error
		*/
		EXEC @ReturnValue = dbo.USP_InsertErrorLog
		SET @ReturnValue = ISNULL(@ReturnValue, @@ERROR)
		GOTO ERROR
	END CATCH

ERROR:
	/* Case 1 UnExpected Error*****/
		-- Assign variables to error-handling functions that
		-- capture information for RAISERROR.
	SELECT @$ErrorNumber = ERROR_NUMBER()
		 , @$ErrorSeverity = ERROR_SEVERITY()
		 , @$ErrorState = ERROR_STATE()
		 , @$ErrorLine = ERROR_LINE()
		 , @$ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-')
		 , @$ErrorMessage = ISNULL(ERROR_MESSAGE(),'NO MESSAGE')

	/* Case 2 Expected Error*****/
	IF @ReturnValue != 0 BEGIN
		SELECT @$ErrorNumber = @ReturnValue
			 , @$ErrorSeverity = 16
			 , @$ErrorState = 1
			 , @$ErrorLine = 0
			 , @$ErrorProcedure = ISNULL(@$prog, '-')
			 , @$ErrorMessage = ISNULL((SELECT Memo FROM @ErrCodeTbl WHERE ErrorCode = @ReturnValue),'Not Defined')
	END
	-- Building the message string that will contain original
		-- error information.
	SELECT @$ErrorMessage = N'Error %d, Level %d, State %d, Procedure %s, Line %d, ' + 'Message: ' + @$ErrorMessage

	IF @$ErrorNumber = 0
		SELECT @$ErrorMessage = 'Unknown'
	IF @$prev_trancount = 0 and @@TRANCOUNT > 0
		Rollback transaction
		-- Raise an error: msg_str parameter of RAISERROR will contain
		-- the original error information.
	RAISERROR (@$ErrorMessage, @$ErrorSeverity, @$ErrorState, @$ErrorNumber, -- parameter: original error number.
	  @$ErrorSeverity, -- parameter: original error severity.
	  @$ErrorState, -- parameter: original error state.
	  @$ErrorProcedure, -- parameter: original error procedure name.
	  @$ErrorLine-- parameter: original error line number.
	)

	RETURN @ReturnValue

DONE:
IF @$prev_trancount = 0 and @@TRANCOUNT > 0 BEGIN
	COMMIT TRANSACTION
END

RETURN @ReturnValue

END