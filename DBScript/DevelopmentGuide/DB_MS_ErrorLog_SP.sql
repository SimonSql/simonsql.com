USE tempdb
GO

CREATE TABLE dbo.ErrorLogs(
    ErrorLogSN				BIGINT           IDENTITY(1,1),
    LoginName				NVARCHAR(128)    NOT NULL,
    HostName				NVARCHAR(128)    NOT NULL,
    ErrorNumber				INT              NOT NULL,
    ErrorSeverity			INT              NULL,
    ErrorState				INT              NULL,
    ErrorProcedure			NVARCHAR(128)    NULL,
    ErrorLine				INT              NULL,
    ErrorMessage			NVARCHAR(4000)    NOT NULL,
	CustomMessage			NVARCHAR(4000)    NOT NULL,
    Registered_DateTime     DATETIME         NOT NULL,
    CONSTRAINT PK_ErrorLogs PRIMARY KEY CLUSTERED (errorLogSN) WITH(DATA_COMPRESSION=PAGE)
)
GO

CREATE PROCEDURE dbo.P_AddErrorLog
@IsRaiseErrorOn BIT = 1
, @IsRaiseErrorWithLogOn BIT = 0			-- Ignored where @IsRaiseErrorOn = 0  
, @IsRollBackTranOn BIT	= 1					-- Ignored where XACT_STATE() = -1 : AlwaysRollback.(Recommanded always turn on "@IsRollBackTranOn=1" since error logging will be rollback as well in parent SP.
, @CustomMessage NVARCHAR(4000) = NULL		-- Anyother message want to store such as parameter and else.
WITH EXECUTE AS OWNER
AS
BEGIN
	SET NOCOUNT ON;
	SET XACT_ABORT ON;

	DECLARE @intReturnValue int;

	DECLARE
		@nvcErrorMessage nvarchar(4000),
		@intErrorNumber int,
		@intErrorSeverity int,
		@intErrorState int,
		@intErrorLine int,
		@nvcErrorProcedure nvarchar(128);

	/**_# Log the error.*/
	/**_## Assign variables to error-handling functions that capture information for RAISERROR.*/
	SET @intErrorNumber = ISNULL(ERROR_NUMBER(),0);
	SET @intErrorSeverity = ISNULL(ERROR_SEVERITY(),0);
	SET @intErrorState = ISNULL(ERROR_STATE(),0);
	SET @intErrorLine = ISNULL(ERROR_LINE(),0);
	SET @nvcErrorProcedure = ISNULL(ERROR_PROCEDURE(),'unknown');
	SET @nvcErrorMessage = ISNULL(ERROR_MESSAGE(),'NULL');

	/**_# If there is no error information to log, return 0.*/
	
	IF @intErrorNumber = 0 BEGIN
		SET @intReturnValue = 0
		/**_## If @CustomMessage is empty, return 0.*/
		IF ISNULL(@CustomMessage,'') = ''
			GOTO Done;
	END
	ELSE BEGIN
		SET @intReturnValue = @intErrorNumber;
	END
	
	/**_# Rollback and return if inside an uncommittable transaction.*/
	IF XACT_STATE() = -1 BEGIN
		--BEGIN TRANSACTION			--Uncommitable transaction, couldn't start new transaction
		ROLLBACK TRANSACTION;
		SET @nvcErrorMessage = '[System : uncommittable transaction] '+ @nvcErrorMessage;
	END

	/**_# Rollback and return if @IsRollBackTranOn and Trancount>0.*/
	IF @IsRollBackTranOn = 1 AND @@TRANCOUNT > 0
	BEGIN
		BEGIN TRANSACTION			--Trick to remove this error: Transaction count after EXECUTE indicates a mismatching number of BEGIN and COMMIT statements. Previous count = 1, current count = 0.
		ROLLBACK TRANSACTION;
	END
    
	/**_## Log the error that occurred.*/
	INSERT dbo.ErrorLogs (LoginName, HostName, ErrorNumber, ErrorSeverity, ErrorState, ErrorProcedure, ErrorLine, ErrorMessage, CustomMessage, Registered_DateTime)
	VALUES (CAST(ORIGINAL_LOGIN() AS nvarchar(128)), CAST(HOST_NAME() AS nvarchar(128)), @intErrorNumber, @intErrorSeverity, @intErrorState, @nvcErrorProcedure, @intErrorLine, @nvcErrorMessage, @CustomMessage, GETDATE());

	/**_# Rethrow the error.*/
	IF @IsRaiseErrorOn = 1 AND @intErrorNumber <> 0 BEGIN
		SET @nvcErrorMessage = N'Error %d, Level %d, State %d, Procedure %s, Line %d, Message: ' + @nvcErrorMessage;

		IF @IsRaiseErrorWithLogOn = 1 BEGIN
			RAISERROR (@nvcErrorMessage, @intErrorSeverity, 1, @intErrorNumber, @intErrorSeverity, @intErrorState, @nvcErrorProcedure, @intErrorLine) WITH LOG;
		END ELSE BEGIN
			RAISERROR (@nvcErrorMessage, @intErrorSeverity, 1, @intErrorNumber, @intErrorSeverity, @intErrorState, @nvcErrorProcedure, @intErrorLine);
		END
	END

Done:
	RETURN @intReturnValue;

END
GO

/****************************************************
Test
****************************************************/
create PROCEDURE usp_test1
AS
BEGIN TRY BEGIN TRAN
	SELECT 1/0
	COMMIT TRAN
END TRY
BEGIN CATCH
	IF @@TRANCOUNT<>0
		ROLLBACK TRAN
	SELECT 1
	DECLARE @CustomMessage NVARCHAR(4000)
	SET @CustomMessage = 'test SP'
	EXEC P_AddErrorLog @IsRaiseErrorOn = 0, @IsRaiseErrorWithLogOn = 0, @IsRollBackTranOn = 1, @CustomMessage = 'Custom'
END CATCH
GO


EXEC usp_test1
go

SELECT * FROM ErrorLogs