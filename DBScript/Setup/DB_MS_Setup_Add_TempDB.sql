use tempdb



ALTER DATABASE [tempdb] MODIFY FILE ( NAME =N'tempdev', FILENAME =N'S:\SQLData\TempDB\tempdev.mdf', SIZE = 1GB, FILEGROWTH = 256MB )
ALTER DATABASE [tempdb] MODIFY FILE ( NAME =N'templog', FILENAME =N'S:\SQLData\TempDB\templog.ldf', SIZE = 1GB, FILEGROWTH = 256MB )
ALTER DATABASE [tempdb] ADD FILE ( NAME =N'tempdev2', FILENAME =N'S:\SQLData\TempDB\tempdev2.ndf', SIZE = 1GB, FILEGROWTH = 256MB )
ALTER DATABASE [tempdb] ADD FILE ( NAME =N'tempdev3', FILENAME =N'S:\SQLData\TempDB\tempdev3.ndf', SIZE = 1GB, FILEGROWTH = 256MB )
ALTER DATABASE [tempdb] ADD FILE ( NAME =N'tempdev4', FILENAME =N'S:\SQLData\TempDB\tempdev4.ndf', SIZE = 1GB, FILEGROWTH = 256MB )

/*
USE [master]
GO

DECLARE @cpu_count int, @file_count int, @logical_name sysname, @file_name nvarchar(520), @physical_name nvarchar(520), @alter_command nvarchar(max)

SELECT @physical_name = physical_name 
FROM tempdb.sys.database_files
WHERE name = 'tempdev'

SELECT @file_count = COUNT(*)
  FROM tempdb.sys.database_files
 WHERE type_desc = 'ROWS'

print @file_count
SELECT @cpu_count = cpu_count
FROM sys.dm_os_sys_info

WHILE (@file_count < @cpu_count )

BEGIN 
SELECT @logical_name = 'tempdev' + CAST(@file_count+1 AS nvarchar) 
SELECT @file_name = REPLACE(@physical_name, 'tempdb.mdf', @logical_name + '.ndf') 
SELECT @alter_command = 'ALTER DATABASE [tempdb] ADD FILE ( NAME =N''' + @logical_name + ''', FILENAME =N''' + @file_name + ''', SIZE = 4GB, FILEGROWTH = 200MB )' 
PRINT @alter_command 
--EXEC sp_executesql @alter_command 
SELECT @file_count = @file_count + 1 
END
*/