use Master
go

exec sp_configure 'show advanced options',1
reconfigure
go
EXEC sys.sp_configure N'min server memory (MB)', N'10240'
GO
EXEC sys.sp_configure N'max server memory (MB)', N'12288'
GO
RECONFIGURE WITH OVERRIDE
go
exec sp_configure 'show advanced options',0
reconfigure
go